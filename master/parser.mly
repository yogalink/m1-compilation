%{

  open Ast

  (* Fonction pour aider à la localisation des erreurs. *)
  let current_pos () =
    Parsing.symbol_start_pos (),
    Parsing.symbol_end_pos ()

  (* Fonction de construction d'un [node_expr], qui renseigne correctement
     la localisation de l'expression. *)
  let mk_node e = { expr = e; pos = current_pos(); typ = Tunknown }

%}

/* Liste des lexèmes, par ordre alphabétique. */
%token AND
%token BOOL
%token COLON
%token <Ast.binop> COMP
%token <bool> CONST_BOOL
%token <int> CONST_INT
%token CONST_UNIT
%token DO
%token DONE
%token DOUBLE_EQUAL
%token ELSE
%token EOF
%token EOI
%token EQUAL
%token FOR
%token <string> IDENT
%token IF
%token IN
%token INT
%token LET
%token LPAREN
%token MINUS
%token NEQ
%token NOT
%token OR
%token PLUS
%token PRINT_INT
%token PRINT_NEWLINE
%token REC
%token RPAREN
%token SEMI
%token SLASH
%token STAR
%token THEN
%token TO
%token BOOL
%token COLON
%token INT
%token UNIT
/* Références */
%token REF
%token GET_REF
%token SET_REF
/* Options */
%token NONE
%token SOME
%token OPTION
/* Structures */
%token TYPE
%token MUTABLE
%token LBRACE
%token RBRACE
%token SEMI
%token DOT
%token LT_MINUS


%nonassoc IN
%nonassoc ELSE
%nonassoc LT_MINUS
%nonassoc SET_REF
%left OR
%left AND
%left COMP EQUAL NEQ DOUBLE_EQUAL
%left PLUS MINUS
%left STAR SLASH
%nonassoc SOME
%left GET_REF
%left DOT
%nonassoc NOT

/* Déclaration de la règle principale et de son type. */
%start prog
%type <Ast.prog> prog

%%

/* Début des règles d'analyse lexicale. */


/* Un programme est une séquence de zéro ou plusieurs instructions, qui est
   représentée par une liste. Pour gérer la répétition (l'étoile dans la
   grammaire), on utilise un non-terminal supplémentaire [instr_seq].
*/
prog:
| instr_seq EOF { $1 }
;

/* Arith */
instr_seq:
| /* empty */    { [] }
| instr instr_seq       { $1 :: $2 }
;


/* Une instruction est une expression suivie par un ;; (EOI). */
instr:
/* Arith */
| expr EOI
    { Icompute $1 }
| LET typed_ident EQUAL expr EOI
    { Ilet (fst $2, snd $2, $4) }
| LET IDENT typed_ident_list COLON typ EQUAL expr EOI
    { Ifun (false, $2, $3, $5, $7) }
| LET REC IDENT typed_ident_list COLON typ EQUAL expr EOI
    { Ifun (true, $3, $4, $6, $8) }
/* Structures */
| TYPE IDENT EQUAL LBRACE field_decl_seq RBRACE EOI
    { Istruct ($2, $5) }

;

/* Structures */
field_decl_seq:
| field_decl
    { [$1] }
| field_decl SEMI field_decl_seq
    { $1 :: $3 }
;

field_decl:
| /* empty */ IDENT COLON typ
    { ($1, Fimmutable $3) }
| MUTABLE IDENT COLON typ
    { ($2, Fmutable $4) }
;


typ:
| UNIT
    { Tunit }
| BOOL
    { Tbool }
| INT
    { Tint }
/* Références */
| typ REF
    { Tref $1 }

/* Options */
| typ OPTION
    { Toption $1 }

/* Structures */
| IDENT
    { Tstruct $1 }

;

typed_ident:
| IDENT COLON typ
    { ($1, $3) }

typed_ident_list:
| LPAREN typed_ident RPAREN
    { [$2] }
| LPAREN typed_ident RPAREN typed_ident_list
    { $2 :: $4 }
;

simple_expr:
| const
    { mk_node $1 }
| LPAREN expr RPAREN
    { $2 }
| IDENT
    { mk_node (Eident $1) }
/* Références */
| GET_REF simple_expr
    { mk_node (Egetref $2) }

/* Structures */
| LBRACE field_def_seq RBRACE
    { mk_node (Estruct $2) }
| field_expr
    { let e, id = $1 in mk_node (Eget (e, id)) }



/* Les expressions sont de trois sortes :
   1/ Des "expressions simples", qui sont soit des constantes soit des
      expressions délimitées par des parenthèses.
   2/ La combinaisons d'une ou plusieurs expressions par des opérateurs
      arithmétiques.
   3/ L'application d'une fonction primitive [print_int] ou [print_newline]
      à une expression simple.
*/
      
expr:
| simple_expr
    { $1 }

| MINUS expr
    { mk_node (Eunop (Uminus, $2)) }
| expr PLUS expr
    { mk_node (Ebinop (Badd, $1, $3)) }
| expr MINUS expr
    { mk_node (Ebinop (Bsub, $1, $3)) }
| expr STAR expr
    { mk_node (Ebinop (Bmul, $1, $3)) }
| expr SLASH expr
    { mk_node (Ebinop (Bdiv, $1, $3)) }

/* Bool */
| IF expr THEN expr ELSE expr
    { mk_node (Eif ($2, $4, $6)) }
| NOT expr
    { mk_node (Eunop (Unot, $2)) }
| expr DOUBLE_EQUAL expr
    { mk_node (Ebinop (Beq, $1, $3)) }
| expr NEQ expr
    { mk_node (Ebinop (Bneq, $1, $3)) }
| expr COMP expr
    { mk_node (Ebinop ($2, $1, $3)) }
| expr AND expr
    { mk_node (Ebinop (Band, $1, $3)) }
| expr OR expr
    { mk_node (Ebinop (Bor, $1, $3)) }
/* Declaration de bloc local */
| LET IDENT EQUAL expr IN expr
    { mk_node (Eletin ($2, $4, $6)) }
| PRINT_INT simple_expr
    { mk_node (Eprint_int $2) }
| PRINT_NEWLINE simple_expr
    { mk_node (Eprint_newline $2) }    
/* Boucle  */
| FOR IDENT EQUAL expr TO expr DO expr DONE
    { mk_node (Efor($2, $4, $6, $8))}
| IDENT args
    { mk_node (Eapp ($1, $2)) }

/* Références */
| REF simple_expr
    { mk_node (Eunop (Uref, $2)) }
| expr SET_REF expr
    { mk_node (Esetref ($1, $3)) }


/* Options */
| SOME expr
    { mk_node (Eunop (Usome, $2)) }
| LET SOME IDENT EQUAL expr IN expr
    { mk_node (Eletopt ($3, $5, $7)) }


/* Structures */
| field_expr LT_MINUS expr
    { let e, id = $1 in mk_node (Eset (e, id, $3)) }


;

/* Structures */
field_expr:
| simple_expr DOT IDENT
    { ($1, $3) }
;


const:
| CONST_UNIT
    { Econst Cunit }
| CONST_BOOL
    { Econst (Cbool $1) }
| CONST_INT
    { Econst (Cint $1) }
/* Options */
| NONE
    { Econst Cnone }

;

/* Structures */
field_def_seq:
| field_def
    { [$1] }
| field_def SEMI field_def_seq
    { $1 :: $3 }
;

field_def:
| IDENT EQUAL expr
    { ($1, $3) }
;


args:
| simple_expr
    { [$1] }
| simple_expr args
    { $1 :: $2 }
;
