open Ast
open Mips
open Format
open Error
(* Constantes pour la représentation des données. *)
let word_size   = 4
let data_size   = word_size
let header_size = word_size

let not_implemented() = failwith "Not implemented"

(* Création d'une nouvelle étiquette pour les branchements. *)
let new_label =
  let c = ref 0 in
  fun () -> incr c; sprintf "__label__%05i" !c

(* Définition de l'environnement. *)
module Env = Map.Make(String)

(* L'environnement contient des variables globales ou locales, et des
   paramètres de fonctions. Chacun est associé à un numéro (son numéro
   d'ordre d'apparition parmi les éléments visibles à cet endroit).
   Le décalage correspondant au numéro d'ordre est calculé différemment en
   fonction du type de variable. *)
type var_loc =
  | Global_var of int
  | Local_var  of int
  | Parameter  of int

let get_var_offset = function
  | Global_var k ->   k    * data_size
  | Local_var  k -> - k    * data_size
  | Parameter  k ->  (k+3) * data_size

let field_offset_tbl = Hashtbl.create 17 (* (struct, field_name) -> offset *)
let struct_size_tbl  = Hashtbl.create 17 (* struct -> size *)
  
(* val push : register -> text 
  [push reg] place le contenu de [reg] au sommet de la pile.
  $sp pointe sur l'adresse de la dernière case occupée. *)
let push reg =
  sub sp sp oi word_size
  @@ sw reg areg (0, sp)

(* val peek : register -> text
  [peek reg] place la valeur en sommet de pile dans [reg] sans dépiler. *)
let peek reg =
  lw reg areg (data_size - 4, sp)

(* val pop : register -> text
  [pop reg] place la valeur en sommet de pile dans [reg] et dépile. *)
let pop reg =
  lw reg areg (0, sp)
  @@ add sp sp oi data_size

(* Allocation dans le tas d'un nombre de mot donné en argument. *)
(* Utilise les registres [v0], [v1], [s0],
   place au sommet de la pile l'adresse du bloc alloué. *)
(* Le programme termine si la limite est dépassée. *)
let malloc size =
     la s0 alab "nxt_loc"   (* Sauvegarde de l'adresse de début du bloc. *)
  @@ lw   v0 areg (0, s0)
  @@ push v0                
  @@ add  v0 v0 oi size     (* Calcul de l'adresse de fin. Arrêt si dépassement. *)
  @@ la  v1 alab "max_loc"
  @@ lw  v1 areg (0, v1)
  @@ bgt v0 v1 "out_of_memory"
  @@ sw  v0 areg (0, s0)    (* Allocation confirmée : modification de nxt_loc. *)

    
(* La fonction de compilation doit prendre en argument, en plus de
   l'expression :
   . l'environnement des variables (pour les deux extensions)
   . le numéro de la prochaine variable locale (pour l'extension des variables
     locales)
   Le type de [compile_expr] doit donc être
   venv -> int -> node_expr -> text
*)

let rec compile_expr env nxt_var e =
  match e.expr with

    | Econst c -> begin
      match c with
	| Cint i      -> li v0 i @@ push v0
	| Cbool true  -> li v0 1 @@ push v0
	| Cunit       -> push zero
	| Cbool false -> push zero
	| Cnone       -> push zero
	| _           -> not_implemented()
    end

    | Eident id ->
      let var = Env.find id env in
      let offset = get_var_offset var in
      let load = match var with
	| Global_var k -> lw v0 areg (offset, gp)
	| Local_var k  
	| Parameter k -> lw v0 areg (offset, fp)
           (*
        | Reference a -> lw v0 areg (data_size, a)
        | Option    a -> lw v0 areg (data_size, a) 
           *)
	in
	load @@ push v0

    | Eunop (op, e) ->
      let e_code = compile_expr env nxt_var e in
      e_code
      @@ pop v0
      @@ (match op with
	| Unot   -> li v1 1 @@ sub v0 v1 oreg v0
	| Uminus -> sub v0 zero oreg v0
        | Usome | Uref ->
           let e_code = compile_expr env nxt_var e in
           e_code
           @@ malloc (header_size + data_size)
           @@ pop v0 (*addr*)
	   @@ li v1 1 (* on donne dans le header le nombre de champs qui est = 1*)
           @@ sw v1 areg(0, v0)
           @@ pop v1 (*result*)
           @@ sw v1 areg(data_size, v0)
      ) @@ push v0

    | Ebinop ((Band | Bor) as op, e1, e2) ->
      let e1_code = compile_expr env nxt_var e1
      and e2_code = compile_expr env nxt_var e2
      and lbl_end = new_label ()
      in
      e1_code
      @@ peek v0
      @@ (match op with
	| Band -> beqz v0 lbl_end
	| Bor  -> bnez v0 lbl_end
	| _    -> assert false)
      @@ pop zero
      @@ e2_code
      @@ label lbl_end

    | Ebinop (op, e1, e2) ->
      let e1_code = compile_expr env (nxt_var + 1) e1
      and e2_code = compile_expr env nxt_var e2
      in
      e2_code
      @@ e1_code
      @@ pop v0
      @@ pop v1
      @@ (match op with
	| Beq  -> seq v0 v0 v1
	| Bneq -> sne v0 v0 v1
	| Blt  -> slt v0 v0 v1
	| Ble  -> sle v0 v0 v1
	| Bgt  -> sgt v0 v0 v1
	| Bge  -> sge v0 v0 v1
	| Badd -> add v0 v0 oreg v1
	| Bsub -> sub v0 v0 oreg v1
	| Bmul -> mul v0 v0 oreg v1
	| Bdiv -> div v0 v0 oreg v1
	| _    -> assert false
      ) @@ push v0
	
    | Eif (cond, e_then, e_else) ->
      let cond_code = compile_expr env nxt_var cond
      and then_code = compile_expr env nxt_var e_then
      and else_code = compile_expr env nxt_var e_else
      and lbl_else  = new_label ()
      and lbl_end   = new_label ()
      in
      cond_code
      @@ pop v0
      @@ beqz v0 lbl_else
      @@ then_code
      @@ b lbl_end
      @@ label lbl_else
      @@ else_code
      @@ label lbl_end

    | Eletin (id, e1, e2) ->
      let e1_code  = compile_expr env nxt_var e1
      and env2     = Env.add id (Local_var nxt_var) env
      and nxt_var2 = nxt_var + 1
      in
      let e2_code = compile_expr env2 nxt_var2 e2 in
      (* Code pour la désallocation de la variable locale. *)
      let desalloc_code = pop v0 @@ pop zero @@ push v0 in
      (* Calcule [e1] et empile le résultat. *)
      e1_code
      (* Calcule [e2] et empile le résultat, en utilisant [e1]
	 comme variable locale. *)
      @@ e2_code
      (* Désalloue le résultat de [e1] et déplace le résultat de [e2]. *)
      @@ desalloc_code

    | Eapp (id, args) ->
      (* On empile les arguments de [a_n] à [a_1].
	 On exécute le corps de la fonction (qui s'occupera de la sauvegarde
	 de [ra] et [fp]).
	 On prend le résultat et on le recopie à la place de [a_n] avant
	 de rendre la main. *)
      let args_code, _ = compile_args env nxt_var args in
      args_code
      @@ jal id
      (* Récupère le résultat résultat au sommet de la pile. *)
      @@ pop v0
      (* On suppose que la fonction appelée a placé le résultat à la place
	 des sauvegardes de [ra] et [fp], et on nettoie les arguments.
	 Potentiellement, on pourrait tout faire faire par l'appelée. *)
      @@ add sp sp oi (data_size * List.length args)
      @@ push v0

    | Eprint_int e ->
      let e_code = compile_expr env nxt_var e in
      e_code
      @@ jal "print_int"
      
    | Eprint_newline e ->
      let e_code = compile_expr env nxt_var e in
      e_code
      @@ jal "print_newline"

    | Egetref e ->
       let e_code = compile_expr env nxt_var e in
       e_code
       @@ pop v0
       @@ lw v1 areg(data_size, v0)
       @@ push v1
    | Esetref (e1, e2) ->
       let e1_code = compile_expr env nxt_var e1 in
       let e2_code = compile_expr env nxt_var e2 in
       e1_code
       @@ e2_code
       @@ pop v0
       @@ pop v1
       @@ sw v0 areg(data_size, v1)
       @@ push zero
    | Eletopt (id, e1, e2) ->
      let e1_code  = compile_expr env nxt_var e1
      and env2     = Env.add id (Local_var nxt_var) env
      and nxt_var2 = nxt_var + 1
      in
      let e2_code = compile_expr env2 nxt_var2 e2 in
      let getvalue = pop v0 @@ lw v1 areg (data_size,v0) @@ push v1 in
      e1_code
      @@ getvalue
      @@ e2_code
    | Estruct def_seq ->
       let n_fields = List.length def_seq in
       let compatible_struct = Typing.find_compatible_struct def_seq in
       let ssize = Hashtbl.find struct_size_tbl (fst compatible_struct) in
       let seq_code = List.fold_left (fun acc elt ->
         let elt_offset = Hashtbl.find field_offset_tbl (fst compatible_struct, fst elt) in
         let elt_code = compile_expr env nxt_var (snd elt) in
         acc @@ elt_code @@ pop v0 @@ sw v0 areg(elt_offset, t0)
         ) nop def_seq in
       malloc ssize
       @@ pop v0
       @@ move t0 v0
       @@ li v1 n_fields
       @@ sw v1 areg(0, v0)
       @@ seq_code
    | Eget (e, id) ->
       let struct_id = match e.typ with
         | Tstruct s -> s
         | t -> error (Not_a_struct_type t) e.pos
       in
       let offset =
         try Hashtbl.find field_offset_tbl (struct_id, id)
         with Not_found -> error (Unknown_field_name id) e.pos
       in
       let e_code = compile_expr env nxt_var e in
       e_code
       @@ pop v0
       @@ lw v1 areg(offset, v0)
       @@ push v1
    | Eset (e1, id, e2) ->
       let struct_id = match e1.typ with
         | Tstruct s -> s
         | t -> error (Not_a_struct_type t) e1.pos
       in
       let offset =
         try Hashtbl.find field_offset_tbl (struct_id, id)
         with Not_found -> error (Unknown_field_name id) e1.pos
       in
       let e1_code = compile_expr env nxt_var e1 in
       let e2_code = compile_expr env nxt_var e2 in       
       e1_code
       @@ e2_code
       @@ pop v1
       @@ pop v0
       @@ sw v1 areg(offset, v0)
    | Efor (id, start_expr, end_expr, loop_expr) -> 
       not_implemented()
	 
and compile_args env nxt_var args =
  match args with
    | []        -> nop, nxt_var
    | a :: args -> let args_code, nxt_var =
		     compile_args env nxt_var args in
		   let a_code = compile_expr env nxt_var a in
		   args_code @@ a_code, nxt_var + 1
(* Les instructions sont calculées l'une après l'autre. *)
let rec compile_instr_list env nxt_global il =
  match il with
    | []       -> nop, nop, nxt_global

    | Icompute e :: il ->
      let e_code  = compile_expr env 0 e in
      let il_code, il_fun_code, glob = compile_instr_list env nxt_global il in
      e_code @@ pop zero @@ il_code, il_fun_code, glob

    | Ilet (id, ty, e) :: il ->
      let e_code = compile_expr env 0 e in
      let offset = get_var_offset (Global_var nxt_global) in
      let alloc_code = pop v0 @@ sw v0 areg (offset, gp)
      in
      let env = Env.add id (Global_var nxt_global) env
      and nxt_global = nxt_global + 1
      in
      let il_code, il_fun_code, glob = compile_instr_list env nxt_global il in
      e_code @@ alloc_code @@ il_code, il_fun_code, glob

    | Ifun (_, id, params, ty, e) :: il ->
      let f_env, nb_params =
	List.fold_left (fun (env, nxt_param) (id, ty) ->
	  Env.add id (Parameter nxt_param) env, nxt_param + 1
	) (env, 0) params
      in
      let e_code = compile_expr f_env 0 e in
      let f_code =
	label id
	@@ push ra (* Sauvegarde de [fp] et [ra]. *)
	@@ push fp
	@@ sub fp sp oi data_size
	(* Définition du nouveau [fp], qui pointe sur le mot de la pile
	   immédiatement sous celui où est stocké l'ancien [fp]. *)
	@@ e_code
	@@ pop v0 (* Sauvegarde du résultat. *)
	@@ pop fp
	@@ pop ra
	@@ push v0 (* Restauration du résultat. *)
	@@ jr ra
      in
      let il_code, il_fun_code, glob = compile_instr_list env nxt_global il in
      il_code, il_fun_code @@ f_code, glob
    | Istruct(struct_id, decl_seq) :: il ->
       let struct_size = List.fold_left ( fun acc elt ->
         Hashtbl.add field_offset_tbl (struct_id, fst elt) acc;
         acc + data_size
       ) header_size decl_seq in
       Hashtbl.add struct_size_tbl struct_id struct_size;
       let il_code, il_fun_code, glob = compile_instr_list env nxt_global il in
       il_code, il_fun_code, glob 
    | _ -> not_implemented()


let init glob =
     sub  sp sp oi (data_size * glob)
  @@ move gp sp
  @@ sub  fp sp oi data_size
  @@ li a0 1024            (* Appel système sbrk pour réserver 1024 octets. *)
  @@ li v0 9
  @@ syscall    
  @@ la a0 alab "nxt_loc"  (* L'appel système a placé dans v0 l'adresse de début
                              de la zone réservée, à mettre dans nxt_loc. *)
  @@ sw v0 areg (0, a0)
  @@ add v0 v0 oi 1024     (* Calcul de max_loc, 1024 octets plus loin. *)
  @@ la a0 alab "max_loc"
  @@ sw v0 areg (0, a0)

let alloc_vars() =
     label "nxt_loc"
  @@ dword [0]
  @@ label "max_loc"
  @@ dword [0]

let built_ins () =
  (* Pour [print_int] et [print_newline] on se passe du cadre d'activation. *)
  label "print_int"
  @@ pop a0
  @@ li v0 1
  @@ syscall
  @@ push zero
  @@ jr ra

  @@ label "print_newline"
  @@ pop zero
  @@ li v0 11
  @@ li a0 10
  @@ syscall
  @@ push zero
  @@ jr ra

  (* Arrêt d'urgence en cas de dépassement de la capacité du tas. *)
  @@ label "out_of_memory"
  @@ la a0 alab "__const_out_of_memory"
  @@ li v0 4
  @@ syscall
  @@ b "end_exec"

let constants () =
     label  "__const_out_of_memory"
  @@ asciiz "out of memory"

let compile_prog p =
  let main_code, fun_code, glob = compile_instr_list Env.empty 0 p in
  let init_code = init glob in
  let built_ins_code = built_ins () in
  { text =
     comment "Initialisation"
  @@ init_code
  @@ comment "Code principal"
  @@ label "main"
  @@ main_code
  @@ comment "Fin"
  @@ label "end_exec"
  @@ li v0 10
  @@ syscall
  @@ comment "Fonctions"
  @@ fun_code
  @@ comment "Primitives"
  @@ built_ins_code
  ;
    
    data =
       comment "Constantes"
    @@ constants ()
    @@ comment "Variables globales pour la gestion du tas"
    @@ alloc_vars ()
  }
