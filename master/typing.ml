open Ast
open Error

(* Informations données par l'environnement de typage. *)
type type_annot =
  | TA_var of typ
  | TA_fun of typ * (typ list)

(* Type de l'environnement de typage. *)
module Env = Map.Make(String)
type  tenv = type_annot Env.t
(* Environnement vide. *)
let empty_env = Env.empty
(* Environnement de base, à compléter pour tenir compte d'éventuelles fonctions
   ou variables primitives, par exemple [print_int] et [print_newline] si
   vous en avez fait des fonctions ordinaires (extension "uniformisation" du
   TP précédent). *)
let base_env = empty_env

let struct_env  = Hashtbl.create 17
(* type senv = Ast.ident (Ast.ident * Ast.field_typ) list Hashtbl.t
   let fields_indx_tbl = Hashtbl.create 17
   Hashtable ne supporte pas des listes de longueur variable? *)

let dummy_pos = (Lexing.dummy_pos, Lexing.dummy_pos)

let get_field_type struct_id field = 
  let struct_def : (Ast.ident * Ast.field_typ) list =
    try Hashtbl.find struct_env struct_id 
    with Not_found -> error (Structure_identifier struct_id) dummy_pos in
  let field_typ : (Ast.ident * Ast.field_typ) =
    try List.find (fun field_tupl -> (fst field_tupl) = field ) struct_def
    with Not_found -> error (Unknown_field_name field) dummy_pos
  in snd field_typ
      
(* Ajout à un nœud de l'information de type. *)
let upd_type ne ty = ne.typ <- ty

(* Fonction à appeler pour rapporter des erreurs de types. *)	
let type_error l t1 t2 = error (Type_error(t1, t2)) l

let not_implemented() = failwith "Not implemented"

let rec check_types l ty1 ty2 = 
  match ty1, ty2 with
  | Tunit, Tunit | Tbool, Tbool | Tint, Tint | Tnone, Tnone -> ()
  | Tnone, Toption opt | Toption opt, Tnone -> () (*check_types l Tnone opt*)
  | Tref r1, Tref r2 -> check_types l r1 r2
  | Toption t1, Toption t2 -> check_types l t1 t2
  | Tstruct t1, Tstruct t2 ->
     begin
       if (Hashtbl.mem struct_env t1) &&  (Hashtbl.mem struct_env t2) && ty1 <> ty2 then
         type_error l ty1 ty2
       else ()
     end
  | _, _ -> type_error l ty1 ty2


(* TODO: Greedy (custom rec) hashtbl fold *)
let find_compatible_struct def_seq =
  let structs_lst = Hashtbl.fold (fun struct_id struct_def l ->
    let is_compatible = List.for_all2 (fun (id1, e1) (id2, fld_typ) ->
      let typ = match fld_typ with
        | Fmutable t -> t
        | Fimmutable t -> t
         in check_types e1.pos e1.typ typ; id1 = id2
    ) def_seq struct_def in
    if is_compatible then (struct_id, struct_def) :: l else l
  ) struct_env []
  in match structs_lst with
  | [] -> failwith("No compatible structure found") 
  | lst -> List.hd lst


let rec type_expr t_env  ne =
  match ne.expr with
  | Efor(_,_,_,_) -> not_implemented()
  | Econst Cunit     -> upd_type ne Tunit
  | Econst (Cint i)  -> upd_type ne Tint
  | Econst (Cbool b) -> upd_type ne Tbool
  | Econst  Cnone    -> upd_type ne Tnone
  | Eident id -> begin
    try
      match Env.find id t_env with
      | TA_var ty -> upd_type ne ty
      | TA_fun _  -> error (Function_identifier id) ne.pos
    with Not_found  -> error (Unknown_identifier id)  ne.pos
  end
      
    | Eunop (op, e) ->
      type_expr t_env  e;
      let ty = begin
	match op with
	| Unot   -> check_types e.pos Tbool e.typ; Tbool
	| Uminus -> check_types e.pos Tint e.typ;  Tint
        | Uref   -> Tref e.typ
        | Usome  -> Toption e.typ
      end in
      upd_type ne ty

    | Ebinop (op, e1, e2) ->
      type_expr t_env e1;
      type_expr t_env e2;
      let ty = begin
	match op with
	  | Beq | Bneq ->
	    check_types e2.pos e1.typ e2.typ;
	    Tbool
	  | Blt | Ble | Bgt | Bge ->
	    check_types e1.pos Tint   e1.typ;
	    check_types e2.pos Tint   e2.typ;
	    Tbool
	  | Badd | Bsub | Bmul | Bdiv ->
	    check_types e1.pos Tint   e1.typ;
	    check_types e2.pos Tint   e2.typ;
	    Tint
	  | Band | Bor ->
	    check_types e1.pos Tbool  e1.typ;
	    check_types e2.pos Tbool  e2.typ;
	    Tbool
      end in
      upd_type ne ty

    | Eif (e1, e2, e3) ->
      type_expr t_env e1;
      type_expr t_env e2;
      type_expr t_env e3;
      let ty =
	check_types e1.pos Tbool  e1.typ;
	check_types e2.pos e2.typ e3.typ;
	e2.typ
      in
      upd_type ne ty

    | Eletin (id, e1, e2) ->
      type_expr t_env e1;
      type_expr (Env.add id (TA_var e1.typ) t_env) e2;
      upd_type ne e2.typ

    | Eapp (id, args) -> begin
      let ty = try Env.find id t_env
	with Not_found -> error (Unknown_identifier id) ne.pos
      in
      match ty with
	| TA_fun (ty, ty_args) ->
	  List.iter2 (fun a ty_a ->
	    type_expr t_env a;
	    check_types a.pos ty_a a.typ
	  ) args ty_args;
	  upd_type ne ty
	| _ -> error (Not_a_function id) ne.pos
    end

    | Eprint_int e ->
      type_expr t_env e;
      check_types e.pos Tint e.typ;
      upd_type ne Tunit

    | Eprint_newline e ->
      (* Calcule le type de l'expression [e], et met à jour le
	 nœud représentant [e]. *)
      type_expr t_env e;
      (* Vérifie que le type trouvé pour [e] est bien le type
	 attendu [Tunit]. *)
      check_types e.pos Tunit e.typ;
      (* Met à jour le type de l'expression complète. *)
      upd_type ne Tunit
             
     | Egetref e ->
        type_expr t_env e;
       begin
         match e.typ with
       | Tref t -> upd_type ne t
       | typ -> type_error e.pos typ (Tref typ)
       end
     | Esetref (e1, e2) ->
        type_expr t_env  e1;
        type_expr t_env  e2;
        check_types e1.pos e1.typ (Tref e2.typ);
        upd_type ne Tunit
     | Eletopt (id, e1, e2) ->
        type_expr t_env  e1;
       let e1_typ = match e1.typ with
         | Toption t -> t
         | Tnone -> failwith "no value"
         | t -> t
       in
       type_expr (Env.add id (TA_var e1_typ) t_env)  e2;
       upd_type ne e2.typ
     | Estruct def_seq ->
       List.iter (fun (_, e) -> type_expr t_env e) def_seq;
       let compatible_struct = find_compatible_struct def_seq in (* TODO: wrap in try find.. with Not_found *)
       let struct_id, orig_def = compatible_struct in
       List.iter2 (fun (_, e) (_, field_typ) ->
         let typ = match field_typ with
           | Fmutable t -> t | Fimmutable t -> t
         in upd_type e typ)
         def_seq orig_def;
       upd_type ne (Tstruct struct_id)
     | Eget (e, field_id) ->
       type_expr t_env e;
       let struct_id = match e.typ with
         | Tstruct sid -> sid
         | typ -> error (Not_a_struct_type typ) ne.pos
       in
       let typ = match get_field_type struct_id field_id with
         | Fmutable t -> t
         | Fimmutable t -> t
       in upd_type ne typ
     | Eset (e1, field_id, e2) ->
       type_expr t_env e1;
       type_expr t_env e2;
       let struct_id = match e1.typ with
         | Tstruct sid -> sid
         | typ -> error (Not_a_struct_type typ) ne.pos
       in
       let _ = match get_field_type struct_id field_id with
         | Fmutable t -> check_types e2.pos t e2.typ
         | Fimmutable t -> error (Not_mutable field_id) ne.pos
       in upd_type ne Tunit
     | _ -> not_implemented()


let type_prog p =
  (* On utilise une fonction auxiliaire, qui type une instruction et
     renvoie l'environnement de typage éventuellement mis à jour. *)
  let rec type_instr_list t_env  instr_list =
    match instr_list with
      | [] -> t_env
	
      | Icompute e :: il ->
	type_expr t_env  e;
	check_types e.pos Tunit e.typ;
	type_instr_list t_env  il
	  
      | Ilet (id, ty, e) :: il -> 
	type_expr t_env e;
	check_types e.pos ty e.typ;
	type_instr_list (Env.add id (TA_var e.typ) t_env) il
	  
      | Ifun (rec_flag, id, args, ty, e) :: il ->
	let f_env = List.fold_left
	  (fun f_env (a, ty_a) -> Env.add a (TA_var ty_a) f_env)
	  t_env args
	in
	let f_env =
	  if rec_flag
	  then Env.add id (TA_fun (ty, List.map snd args)) f_env 
	  else f_env
	in
	type_expr f_env e;
	check_types e.pos ty e.typ;
	type_instr_list
	  (Env.add id (TA_fun (ty, List.map snd args)) t_env)
	  il
      | Istruct(id, decl_seq) :: il ->
         if Hashtbl.mem struct_env id then
           failwith("Structure redefinition attempt")
         else
           Hashtbl.add struct_env id decl_seq;
         (* calculer et enregistrer les shift de chaque field dans une Hashtbl*)
         type_instr_list t_env il
      | _ -> not_implemented()

  in
  type_instr_list base_env p
       (* Remarque : cette fonction renvoie l'environnement de typage final.
	  Ça peut servir dans l'extension "optimisation : valeurs inutiles". *)
