open Ast
open Mips


(* Environnement pour les variables locales, on associe à une variable sa position par rapport à fp *)
module Env = Map.Make(String)

(* A chaque déclaration de structure, on associe les champs avec les décalages par rapport au premier champ, et la taille de la structure *)
let struct_shift = Hashtbl.create 35

(* Ensemble des constantes de type string pour printf *)
let str_cst = Hashtbl.create 97

(* Ensemble des constantes de type double pour printf *)
let float_cst = Hashtbl.create 97

(* Numéro des labels pour garder leur unicité *)
let glob_lab = ref 0

(** Fonction utilitaire qui annote la valeur [e] avec l'information [t] *)
let node e t = { node = e; loc = t }

(* Position utilises lorsque l'on crée artificiellement des noeuds qui n'existent pas dans l'ast *)
let dummy_loc = Lexing.dummy_pos, Lexing.dummy_pos


(* Ajoute le string s s'il n'est pas déjà dans la table des constantes string et renvoie son label data *)
let add_str s =
	try 
		Hashtbl.find str_cst s
	with Not_found ->
		let lab = "str_" ^ (string_of_int !glob_lab) in
		Hashtbl.add str_cst s lab;
    incr (glob_lab);
		lab


(* Ajoute le double d dans la table des constantes doubles et renvoie son label data *)
let add_float d =
	try 
		Hashtbl.find float_cst d
	with Not_found ->
		let lab = "float_" ^ (string_of_float d) in
		Hashtbl.add float_cst d lab;
		lab


(* Détermine le padding nécessaire pour l'alignement *)
let rec padd curr_pos align_voulu =
	if curr_pos mod align_voulu = 0 then
		curr_pos
	else
		padd (curr_pos + 1) align_voulu
		

(* Renvoie le logarithme binaire de l'alignement du type ou du plus grand champ de la structure *)
let rec align_var typ =
	match typ with
	| Tint | Tpointer _ | Tnull -> 2
	| Tchar -> 0
	| Tdouble -> 3
	| Tstruct id ->
		let var_list = Hashtbl.find Typing.struct_env id.node in
		List.fold_left (fun acc (t, _) -> max acc (align_var t)) 0 var_list
	| Tvoid -> assert false


(* Renvoie le nombre d'octets occupés par la représentation du type *)
let rec sizeof typ =
	match typ with
	| Tint | Tpointer _ | Tnull -> 4
	| Tchar -> 1
	| Tdouble -> 8
	| Tstruct id ->
		let var_list = Hashtbl.find Typing.struct_env id.node in
		let size = List.fold_left (fun acc (t, _) -> padd acc (1 lsl align_var t) + sizeof t) 0 var_list in
		padd size (1 lsl align_var typ)
	| Tvoid -> 0


(* Alloue de l'espace, aligne la mémoire et donne une valeur par défaut aux types simples *)
let rec default_type_value t =
	match t with
	| Tint | Tchar | Tpointer _ -> word [0]
	| Tdouble -> double [0.0]
	| Tstruct _ -> align (align_var t) @@ space (sizeof (t))
	| _ -> assert false


(* Copie une structure d'un emplacement mémoire mem1 à un autre emplacement mémoire mem2 *)
let rec memmove name mem1 mem2 =
  let (liste_champs, size) = (Hashtbl.find struct_shift name.node) in
  let copy_mem _ shift acc = acc @@ lw a0 areg (shift, mem1) @@ sw a0 areg (shift, mem2) in
  Hashtbl.fold copy_mem liste_champs nop  


(* Compile une expression *)
let rec compile_expr (env, fpsize, arg_length) e =
  match e.node with
  | Enull -> push zero
  | Econst (Cint i) -> li32 a0 i @@ push a0
  | Econst (Cstring s) -> let str = add_str s in la a0 alab str @@ push a0
  | Econst (Cdouble d) -> let dbl = add_float d in ldc1 f0 alab dbl @@ push f0
  | Esizeof typ -> li a0 (sizeof typ) @@ push a0
  | Eunop (op, e0) ->
  	let code_lexpr () = compile_lexpr (env, fpsize, arg_length) e0 in
    let code_expr () = compile_expr (env, fpsize, arg_length) e0 in
  	(
  	match op with 
    | Upre_inc -> code_lexpr () @@ pop a0 @@ lw a1 areg (0, a0) @@ add a1 a1 oi 1 @@ sw a1 areg (0, a0) @@ push a1
    | Upost_inc -> code_lexpr () @@ pop a0 @@ lw a1 areg (0, a0) @@ add a2 a1 oi 1 @@ sw a2 areg (0, a0) @@ push a1
    | Upre_dec -> code_lexpr () @@ pop a0 @@ lw a1 areg (0, a0) @@ sub a1 a1 oi 1 @@ sw a1 areg (0, a0) @@ push a1
    | Upost_dec -> code_lexpr () @@ pop a0 @@ lw a1 areg (0, a0) @@ sub a2 a1 oi 1 @@ sw a2 areg (0, a0) @@ push a1

    | Uminus ->
    (
      match e.loc with
      | Tdouble -> code_expr () @@ pop f0 @@ neg_d f0 f0 @@ push f0
      | _ -> code_expr () @@ pop a0 @@ neg a0 a0 @@ push a0
    )
    | Uplus -> code_expr ()
    | Unot ->
    (
      match e.loc with
      | Tdouble ->
        let lab = "floatOpT_" ^ (string_of_int !glob_lab) in
        let suite = "floatOpS_" ^ (string_of_int !glob_lab) in
        let lab_1 = add_float 1.0 in
        incr (glob_lab);
        code_expr () @@ pop f0 @@ ldc1 f2 alab "float_0." @@ c_eq_d f0 f2 @@ bc1t lab @@ sub_d f2 f0 f0 @@ b suite @@ label lab @@ ldc1 f2 alab lab_1 @@ label suite @@ push f2
      | _ -> code_expr () @@ pop a0 @@ seq a0 a0 zero @@ push a0
    )
    | Ustar -> 
    (
      match e0.loc with
      | Tstruct _ ->
        (
        if Typing.is_lvalue e0 then 
          code_lexpr ()
        else
          code_expr ()
        )
        @@ pop a0 @@ lw a0 areg (0, a0) @@ push a0
      | _ -> 
        (
        if Typing.is_lvalue e0 then 
          code_lexpr ()
        else
          code_expr ()
        )
        @@ pop a0 @@ lw a0 areg (0, a0) @@ push a0
      )
    | Uamp -> code_lexpr ()
  )

  | Edot (e0, id) ->
    (
    match e0.loc with 
      | Tstruct name ->
        let code_expr = compile_lexpr (env, fpsize, arg_length) e0 in
        let (liste_champs, size) = (Hashtbl.find struct_shift name.node) in
        let shift = Hashtbl.find liste_champs id.node in
        code_expr @@ pop a0 @@ lw a0 areg(size - shift, a0) @@ push a0
      | _ -> assert false
    )

  | Ebinop (op, e1, e2) ->
    (
    let code_expr1 () = compile_expr (env, fpsize, arg_length) e1 in
    let code_expr2 () = compile_expr (env, fpsize, arg_length) e2 in

    match e1.loc with 
    | Tdouble ->
      let darith_op binop = match binop with | Badd -> add_d | Bsub -> sub_d | Bmul -> mul_d | Bdiv -> div_d | _ -> assert false in
      (
      match op with
      | Badd | Bsub | Bmul | Bdiv ->
        code_expr1 () @@ code_expr2 () @@ pop f2 @@ pop f0 @@ (darith_op op) f0 f0 f2 @@ push f0
      | Beq | Bneq | Blt | Ble | Bgt | Bge ->
        let lab = "floatOpT_" ^ (string_of_int !glob_lab) in
        let suite = "floatOpS_" ^ (string_of_int !glob_lab) in
        incr (glob_lab);
        let code =
        code_expr1 () @@ code_expr2 () @@ pop f2 @@ pop f0 @@
        if op = Beq then c_eq_d f0 f2 @@ bc1t lab
        else if op = Bneq then c_eq_d f0 f2 @@ bc1f lab
        else if op = Blt then c_lt_d f0 f2 @@ bc1t lab
        else if op = Ble then c_le_d f0 f2 @@ bc1t lab
        else if op = Bgt then c_le_d f0 f2 @@ bc1f lab
        else if op = Bge then c_lt_d f0 f2 @@ bc1f lab
        else assert false
        in
        code @@ li a0 0 @@ b suite @@ label lab @@ li a0 1 @@ label suite @@ push a0
      | Band | Bor ->
        let lab = "lazy_" ^ (string_of_int !glob_lab) in
        let lab2 = "lazyDone_" ^ (string_of_int !glob_lab) in
        incr (glob_lab);
        let (i1, i2, op) = if op = Band then (1, 0, bc1t) else (0, 1, bc1f) in
        code_expr1 () @@ pop f0 @@ ldc1 f2 alab "float_0." @@ c_eq_d f0 f2 @@ op lab @@
        code_expr2 () @@ pop f0 @@ ldc1 f2 alab "float_0." @@ c_eq_d f0 f2 @@ op lab @@
        li a0 i1 @@ push a0 @@ b lab2 @@ label lab @@
        li a0 i2 @@ push a0 @@ label lab2
      | _ -> assert false
      )
    | _ ->
      let branch_op binop = match binop with | Beq -> seq | Bneq -> sne | Blt -> slt | Ble -> sle | Bgt -> sgt | Bge -> sge | _ -> assert false in
      let arith_op binop = match binop with | Badd -> add | Bsub -> sub | Bmul -> mul | Bdiv -> div | Bmod -> rem | _ -> assert false in
      match op with
      | Badd | Bsub | Bmul | Bdiv | Bmod ->
        code_expr1 () @@ code_expr2 () @@ pop a1 @@ pop a0 @@ (arith_op op) a0 a0 oreg a1 @@ push a0
      | Beq | Bneq | Blt | Ble | Bgt | Bge ->
        code_expr1 () @@ code_expr2 () @@ pop a1 @@ pop a0 @@ (branch_op op) a0 a0 a1 @@ push a0
      | Band | Bor ->
        let lab = "lazy_" ^ (string_of_int !glob_lab) in
        let lab2 = "lazyDone_" ^ (string_of_int !glob_lab) in
        incr (glob_lab);
        let (i1, i2, op) = if op = Band then (1, 0, beqz) else (0, 1, bnez) in
        code_expr1 () @@ pop a0 @@ op a0 lab @@
        code_expr2 () @@ pop a0 @@ op a0 lab @@
        li a0 i1 @@ push a0 @@ b lab2 @@ label lab @@
        li a0 i2 @@ push a0 @@ label lab2
  )

  | Eident id ->
    (
    match e.loc with
    | Tstruct name ->
      let (liste_champs, size) = (Hashtbl.find struct_shift name.node) in
      (
      try
        let shift_id = Env.find id.node env in
        let copy_mem _ shift acc = acc @@ lw a0 areg (shift_id - shift, fp) @@ sw a0 areg (size - shift, sp) in
        sub sp sp oi size @@
        Hashtbl.fold copy_mem liste_champs nop
      with Not_found ->
        let copy_mem _ shift acc = acc @@ lw a0 areg (-shift, a1) @@ sw a0 areg (size - shift, sp) in
        sub sp sp oi size @@ la a1 alab ("glob_" ^ id.node) @@ Hashtbl.fold copy_mem liste_champs nop
      )
    | Tdouble ->
      (
      try
        let shift = Env.find id.node env in ldc1 f0 areg (shift, fp) @@ push f0
      with Not_found ->
        ldc1 f0 alab ("glob_" ^ id.node) @@ push f0
      )
    | _ ->
    	try
    		let shift = Env.find id.node env in lw a0 areg (shift, fp) @@ push a0
    	with Not_found ->
  		  lw a0 alab ("glob_" ^ id.node) @@ push a0
	  )

  | Eassign (e1, e2) ->
    (
    match e.loc with
    | Tstruct name -> (
        let (liste_champs, size) = Hashtbl.find struct_shift name.node in
        match e2.node with
        
        | Eident x -> 
          let code_assign (id: string) _ acc =
            let node_id = node id dummy_loc in
            let t = List.fold_left (fun acc (t, s) -> if s.node = id then t else acc) Tnull (Hashtbl.find Typing.struct_env name.node) in
            acc @@ compile_expr (env, fpsize, arg_length) (node (Eassign (node (Edot (e1, node_id)) t, node (Edot(e2, node_id)) t)) t) @@ pop a0
          in
          Hashtbl.fold code_assign liste_champs nop @@ push a0
        
        | _ ->
          let copy_mem _ shift acc = acc @@ lw a1 areg (size - shift, sp) @@ sw a1 areg (-shift, a0) in
          compile_expr (env, fpsize, arg_length) e2 @@
          compile_lexpr (env, fpsize, arg_length) e1 @@
          pop a0 @@
          Hashtbl.fold copy_mem liste_champs nop
      )
    | Tdouble ->
      let code_lexpr = compile_lexpr (env, fpsize, arg_length) e1 in
      let code_expr = compile_expr (env, fpsize, arg_length) e2 in
      code_lexpr @@ code_expr @@ pop f2 @@ pop a0 @@ sdc1 f2 areg (0, a0) @@ push a0
    | _ ->
      let code_lexpr = compile_lexpr (env, fpsize, arg_length) e1 in
      let code_expr = compile_expr (env, fpsize, arg_length) e2 in
      code_lexpr @@ code_expr @@ pop a1 @@ pop a0 @@ sw a1 areg (0, a0) @@ push a0
    )
  | Ecall (f, [ _; e2 ]) when f.node = "printf" ->
    let code_e2 = compile_expr (env, fpsize, arg_length) e2 in
    code_e2 @@ (
      match e2.loc with
      | Tint | Tnull -> pop a0 @@ li v0 1 @@ syscall @@ push zero
      | Tpointer (Tchar) -> pop a0 @@ li v0 4 @@ syscall @@ push zero
      | Tdouble -> pop f12 @@ li v0 3 @@ syscall @@ push zero
      | _ -> assert false
	 )
  | Ecall (f, [esize]) when f.node = "sbrk" ->
    let code_expr = compile_expr (env, fpsize, arg_length) esize in
    code_expr @@ pop a0 @@ li v0 9 @@ syscall @@ push v0
  | Ecall (f, lexpr) ->
    match e.loc with
    | _ ->
      let lab = "fun_" ^ f.node in
      let size = padd (sizeof e.loc) 8 in
      let (empile_arg, taille_arg) =
      List.fold_left (fun (code, taille) e -> (code @@ compile_expr (env, fpsize, arg_length) e), padd (sizeof e.loc) 8 + taille) (nop, 0) lexpr in
      comment "alloc_res" @@
      sub sp sp oi size
      @@ empile_arg
      @@ jal lab @@ comment "dessaloc_arg" @@ add sp sp oi taille_arg @@ if e.loc = Tvoid then push zero else nop


(* Compile une expression de valeur gauche *)
and compile_lexpr (env, fpsize, arg_length) e = 
  match e.node with
  | Eident x ->
    (
    try
      let shift = Env.find x.node env in
      la a0 areg (shift, fp) @@ push a0
    with Not_found ->
      la a0 alab ("glob_" ^ x.node) @@ push a0
    )
  | Eunop (Ustar, e0) -> compile_lexpr (env, fpsize, arg_length) e0
  | Edot (e0, id) ->
    (
    match e0.loc with
      | Tstruct name ->
        let code_expr = compile_lexpr (env, fpsize, arg_length) e0 in
        let (liste_champs, size) = (Hashtbl.find struct_shift name.node) in
        let shift = Hashtbl.find liste_champs id.node in
        code_expr @@ pop a0 @@ la a0 areg(size - shift, a0) @@ push a0
      | _ -> assert false
    )
  | _ -> assert false


(* fpsize est la différence entre sp et fp, fp ne bouge pas donc on l'utilise comme référence *)
let add_decls (env, fpsize, arg_length) decl_list =
	let add_decl (env_acc, pos) (typ, id) =
		let size = sizeof typ in
		(Env.add id.node (-pos) env_acc, padd (pos + size ) 8)
	in let (new_env, new_fpSize) = List.fold_left add_decl (env, fpsize) decl_list in
	let alloc_val = new_fpSize - fpsize in
	((new_env, new_fpSize, arg_length), alloc_val)


(* Compile une instruction *)
let rec compile_instr (env, fpsize, arg_length) f i =
  let code_expr e = compile_expr (env, fpsize, arg_length) e in
  let code_instr i = compile_instr (env, fpsize, arg_length) f i in
  let new_lab () = 
    let lab = "branch_" ^ (string_of_int !glob_lab) in
    let lab2 = "suite_" ^ (string_of_int !glob_lab) in
    incr (glob_lab);
    (lab, lab2)
  in
  match i.node with
  | Sskip -> nop
  | Sexpr e ->
  (
    code_expr e @@
    match e.loc with
    | Tstruct name -> add sp sp oi (snd (Hashtbl.find struct_shift name.node))
    | _ -> pop a0
  )
  | Sblock b -> compile_block (env, fpsize, arg_length) f b
  | Sreturn eopt -> (
      match eopt with
      | None -> nop
      | Some e -> (
        match e.loc with
        | Tstruct name ->
          let (liste_champs, size) = Hashtbl.find struct_shift name.node in
          compile_expr (env, fpsize, arg_length) e @@ Hashtbl.fold (fun _ shift acc -> acc @@ lw a0 areg (size - shift, sp) @@ sw a0 areg ((arg_length + size - shift), fp)) liste_champs nop @@ add sp sp oi size
        | Tdouble -> compile_expr (env, fpsize, arg_length) e @@ pop f0 @@ sdc1 f0 areg (arg_length, fp)
        | _ -> compile_expr (env, fpsize, arg_length) e @@ pop a0 @@ sw a0 areg (arg_length, fp)
      )
    )
     @@ comment "beginRet" @@ add sp sp oi (fpsize - 16) @@ comment "endRet" @@ b ("fun_" ^ f.node ^ "_return")
  | Sif (e, i1, i2) ->
    let (lab, lab2) = new_lab () in
    code_expr e @@ pop a0 @@ beqz a0 lab @@ code_instr i1 @@ b lab2 @@ label lab @@ code_instr i2 @@ label lab2
  | Swhile (e, i) ->
    let (lab, lab2) = new_lab () in
    label lab @@ code_expr e @@ pop a0 @@ beqz a0 lab2 @@ code_instr i @@ b lab @@ label lab2
  | Sfor (sl1, e, sl2, l) ->
    let compile_instr_list acc i = acc @@ code_instr i in
    let code_linstr1 = List.fold_left compile_instr_list nop sl1 in
    let code_linstr2 = List.fold_left compile_instr_list nop sl2 in
    let (lab, lab2) = new_lab () in
    code_linstr1 @@ label lab @@ code_expr e @@ pop a0 @@ beqz a0 lab2 @@ code_instr l @@ code_linstr2 @@ b lab @@ label lab2

(* Compile un bloc *)
and compile_block (env, fpsize, arg_length) f (vars, instrl) =
  let ((new_env, new_fpSize, arg_length), alloc_val) = add_decls (env, fpsize, arg_length) vars in
  comment "alloc varBlock" @@
  sub sp sp oi alloc_val @@
  List.fold_left (fun acc i ->
      acc @@ compile_instr (new_env, new_fpSize, arg_length) f i) nop instrl @@ comment "desalloc varBlock" @@ add sp sp oi alloc_val


(* Compile une fonction *)
let compile_fun ret id decl_list bloc =
	let size_ret = sizeof ret in
	let size_ret_padd = padd size_ret 8 in
	let ((_, size, _), _) = add_decls (Env.empty, size_ret_padd, 0) decl_list in (* On fait un parcours "fantôme" pour récupérer la taille *)
	let ((env, new_size, arg_length), alloc_arg) = add_decls (Env.empty, (-size+size_ret_padd), size) decl_list in
	let code_body = compile_block (env, new_size + 16, arg_length) id bloc in

	label ("fun_" ^ id.node)
	@@ push fp
	@@ move fp sp
	@@ push ra
	@@ code_body
	@@ label ("fun_" ^ id.node ^ "_return")
	@@ pop ra
	@@ pop fp
	@@ jr ra


(* Compile une déclaration, en particulier celles des fonctions et remplit la table des structures *)
let compile_decl decl =
	match decl with
  	| Dfun (ret, id, dec_list, bloc) -> compile_fun ret id dec_list bloc
    | Dstruct (ident, decl_list) -> 
      let struct_map = Hashtbl.create 5 in
      let champs_map acc (t, id) =
        let shift = padd acc (1 lsl align_var t) + sizeof t in
        Hashtbl.add struct_map id.node shift;
        shift
      in
      let taille_struct = padd (sizeof (Tstruct ident)) 8 in
      let _ = List.fold_left champs_map 0 decl_list in
      Hashtbl.add struct_shift ident.node (struct_map, taille_struct);
      nop
  	| _ -> nop


(* Crée la partie data du programme MIPS *)
let create_data () =
	let create_data id typ acc =
		label ("glob_" ^ id) @@ default_type_value typ @@ acc
	in
	let create_data_str s lab acc =
		label (lab) @@ asciiz s @@ acc
	in
	let create_data_float d lab acc =
		label (lab) @@ double [d] @@ acc
	in
	let h1 = Hashtbl.fold create_data Typing.glob_env nop in
	let h2 = Hashtbl.fold create_data_str str_cst h1 in
  let _ = add_float 0.0 in
	Hashtbl.fold create_data_float float_cst h2


(* Crée la partie texte du programme MIPS *)
let create_text file = 
	let fun_code = List.fold_left (fun acc decl -> acc @@ compile_decl decl) nop file in
		label "main" @@ move a0 sp @@ rem a0 a0 oi 8 @@ sub sp sp oreg a0 @@ (*sub sp sp oi 8 @@*) jal "fun_main" @@ pop a0 @@ li v0 17 @@ syscall @@ fun_code


(* On écrit le code MIPS du fichier, d'abord la partie data, puis la partie text *)
let compile_file file =
	let text = create_text file in
	let data = create_data () in
	{data = data; text = text}