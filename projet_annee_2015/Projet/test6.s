.text
main:
	move $a0, $sp
	rem $a0, $a0, 8
	sub $sp, $sp, $a0
	jal fun_main
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 17
	syscall
fun_max:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	ldc1 $f0, 16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, 8($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.le.d $f0 $f2
	bc1f floatOpT_1
	li $a0, 0
	b floatOpS_1
floatOpT_1:
	li $a0, 1
floatOpS_1:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_0
	ldc1 $f0, 16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f0 0($sp)
	add $sp, $sp, 8
	sdc1 $f0, 24($fp)
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_max_return
	b suite_0
branch_0:
	ldc1 $f0, 8($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f0 0($sp)
	add $sp, $sp, 8
	sdc1 $f0, 24($fp)
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_max_return
suite_0:
#desalloc varBlock
	add $sp, $sp, 0
fun_max_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_main:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 80
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	ldc1 $f0, float_10.5
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sdc1 $f2, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	ldc1 $f0, float_21.
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sdc1 $f2, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.eq.d $f0 $f2
	bc1t floatOpT_6
	li $a0, 0
	b floatOpS_6
floatOpT_6:
	li $a0, 1
floatOpS_6:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_3
	la $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_5
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_3
branch_3:
	la $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_4
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_3:
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.eq.d $f0 $f2
	bc1f floatOpT_10
	li $a0, 0
	b floatOpS_10
floatOpT_10:
	li $a0, 1
floatOpS_10:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_7
	la $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_9
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_7
branch_7:
	la $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_8
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_7:
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.lt.d $f0 $f2
	bc1t floatOpT_14
	li $a0, 0
	b floatOpS_14
floatOpT_14:
	li $a0, 1
floatOpS_14:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_11
	la $a0, -48($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_13
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_11
branch_11:
	la $a0, -48($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_12
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_11:
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.le.d $f0 $f2
	bc1t floatOpT_18
	li $a0, 0
	b floatOpS_18
floatOpT_18:
	li $a0, 1
floatOpS_18:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_15
	la $a0, -56($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_17
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_15
branch_15:
	la $a0, -56($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_16
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_15:
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.le.d $f0 $f2
	bc1f floatOpT_22
	li $a0, 0
	b floatOpS_22
floatOpT_22:
	li $a0, 1
floatOpS_22:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_19
	la $a0, -64($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_21
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_19
branch_19:
	la $a0, -64($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_20
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_19:
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	c.lt.d $f0 $f2
	bc1f floatOpT_26
	li $a0, 0
	b floatOpS_26
floatOpT_26:
	li $a0, 1
floatOpS_26:
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_23
	la $a0, -72($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_25
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_23
branch_23:
	la $a0, -72($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_24
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_23:
	lw $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -48($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -56($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -64($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -72($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	add.d $f0 $f0 $f2
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sdc1 $f2, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -80($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	sub.d $f0 $f0 $f2
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sdc1 $f2, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -80($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	mul.d $f0 $f0 $f2
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sdc1 $f2, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -80($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	l.d $f0 0($sp)
	add $sp, $sp, 8
	div.d $f0 $f0 $f2
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f2 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sdc1 $f2, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -80($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 8
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	ldc1 $f0, -24($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	jal fun_max
#dessaloc_arg
	add $sp, $sp, 16
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, float_0.
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f0 0($sp)
	add $sp, $sp, 8
	ldc1 $f2, float_0.
	c.eq.d $f0 $f2
	bc1f lazy_27
	ldc1 $f0, float_20.
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f0 0($sp)
	add $sp, $sp, 8
	ldc1 $f2, float_0.
	c.eq.d $f0 $f2
	bc1f lazy_27
	li $a0, 0
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	b lazyDone_27
lazy_27:
	li $a0, 1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
lazyDone_27:
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, -16($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f0 0($sp)
	add $sp, $sp, 8
	neg.d $f0 $f0
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	ldc1 $f0, float_0.
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f0 0($sp)
	add $sp, $sp, 8
	ldc1 $f2, float_0.
	c.eq.d $f0 $f2
	bc1t floatOpT_28
	sub.d $f2 $f0 $f0
	b floatOpS_28
floatOpT_28:
	ldc1 $f2, float_1.
floatOpS_28:
	sub $sp, $sp, 8
	s.d $f2 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 80
fun_main_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
.data
float_20.:
	.double 20.000000
float_10.5:
	.double 10.500000
float_1.:
	.double 1.000000
float_0.:
	.double 0.000000
float_21.:
	.double 21.000000
str_4:
	.asciiz "egal: faux"
str_20:
	.asciiz "sup: faux"
str_17:
	.asciiz "infeq: vrai"
str_2:
	.asciiz "\n"
str_12:
	.asciiz "inf: faux"
str_5:
	.asciiz "egal: vrai"
str_16:
	.asciiz "infeq: faux"
str_8:
	.asciiz "different: faux"
str_25:
	.asciiz "supeq: vrai"
str_24:
	.asciiz "supeq: faux"
str_13:
	.asciiz "inf: vrai"
str_21:
	.asciiz "sup: vrai"
str_9:
	.asciiz "different: vrai"
