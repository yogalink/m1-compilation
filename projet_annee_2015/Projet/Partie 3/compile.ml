open Ast
open Mips


(*
let compile_decl d =
	match d with
	| Dvars decl_list -> compile_decl_var decl_list
  	| Dstruct (id, decl_list) -> assert false
  	| Dfun (typ, id, dec_list, bloc) -> assert false
*)

let rec padd curr_pos align_voulu =
	if curr_pos mod align_voulu = 0 then
		curr_pos
	else
		padd (curr_pos + 1) align_voulu

let rec align_var typ =
	match typ with
	| Tint | Tpointer _ | Tnull -> 2
	| Tchar -> 0
	| Tdouble -> 3
	| Tstruct id ->
		let var_list = Hashtbl.find Typing.struct_env id.node in
		List.fold_left (fun acc (t, _) -> max acc (align_var t)) 0 var_list
	| Tvoid -> assert false


let rec sizeof typ =
	match typ with
	| Tint | Tpointer _ | Tnull -> 4
	| Tchar -> 1
	| Tdouble -> 8
	| Tstruct id ->
		let var_list = Hashtbl.find Typing.struct_env id.node in
		let size = List.fold_left (fun acc (t, _) -> padd acc (1 lsl align_var t) + sizeof t) 0 var_list in
		padd size (1 lsl align_var typ)
	| _ -> assert false


let compile_file file =
	let create_data id typ acc =
		acc @@ align (align_var typ) @@ label id @@ space (sizeof (typ))
	in
	let data = Hashtbl.fold create_data Typing.glob_env nop in
	{data = data; text = nop}