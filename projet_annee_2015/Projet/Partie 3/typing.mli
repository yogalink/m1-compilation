val file : Ast.loc Ast.file -> Ast.c_type Ast.file
(** [file f] type l'AST f passé en argument et renvoie un AST contenant le type de chaque noeud dans le champ [loc] *)

val is_lvalue : 'info Ast.expr -> bool
(** [is_lvalue e] renvoie [true] ssi [e] est une valeur gauche *)

val struct_env : (string, Ast.var_decl list) Hashtbl.t
(** [struct_env] est une table de hash contenant pour chaque nom de structure la liste de ses champs.
    Elle n'est initialisée qu'après l'appel à [file]
 *)
val glob_env : (string, Ast.c_type) Hashtbl.t
(** [glob_env] est une table de hash contenant pour chaque variable globale son type.
    Elle n'est initialisée qu'après l'appel à [file]
 *)
val type_to_string : Ast.c_type -> string
(** [type_to_string t] est une fonction auxiliaire permettant de contertir un type en chaine de caractères
 *)
exception Error of Ast.loc * string
