double max(double a, double b) {
	if(a>b)
		return a;
	else 
		return b;
}

int main() {
	double a;
	double b;
	char *c;
	char *d;
	char *e;
	char *f;
	char *g;
	char *h;
	double n;
	int m;

	a = 10.5;
	b = 21.0;
	//a = 21.56;

	printf("%f", a);
	printf("%s", "\n");

	printf("%f", b);
	printf("%s", "\n");

	if(a == b)
		c = "egal: vrai";
	else
		c = "egal: faux";
	if(a != b)
		d = "different: vrai";
	else
		d = "different: faux";
	if(a < b)
		e = "inf: vrai";
	else
		e = "inf: faux";
	if(a <= b)
		f = "infeq: vrai";
	else
		f = "infeq: faux";
	if(a > b)
		g = "sup: vrai";
	else
		g = "sup: faux";
	if(a >= b)
		h = "supeq: vrai";
	else
		h = "supeq: faux";

	printf("%s", c);
	printf("%s", "\n");
	printf("%s", d);
	printf("%s", "\n");
	printf("%s", e);
	printf("%s", "\n");
	printf("%s", f);
	printf("%s", "\n");
	printf("%s", g);
	printf("%s", "\n");
	printf("%s", h);
	printf("%s", "\n");

	n = a + b;
	printf("%f", n);
	printf("%s", "\n");

	n = a - b;
	printf("%f", n);
	printf("%s", "\n");

	n = a * b;
	printf("%f", n);
	printf("%s", "\n");

	n = a / b;
	printf("%f", n);
	printf("%s", "\n");

	printf("%f", max(a, b));
	printf("%s", "\n");

	printf("%d", (0.0) || (20.0));
	printf("%s", "\n");

	printf("%f", -a);
	printf("%s", "\n");

	printf("%f", !0.0);
	printf("%s", "\n");


/*
	m = (a == 0) || (b == 20);
	printf("%d", m);
	printf("%s", "\n");*/
}