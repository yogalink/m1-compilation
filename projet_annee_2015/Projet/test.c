
struct S {
	int a;
	char b;
	char c;
	double p;
};

struct T {
	int x;
	char y;
};

struct S s;
struct T t;

int n;
double d;
int x;

int main(int argc, char **argv) {
	n = 2;
	x = 50;
	printf("%d", n++);
	printf("%s", "\n");

	printf("%d", n);
	printf("%s", "\n");

	printf("%d", n--);
	printf("%s", "\n");

	printf("%d", n);
	printf("%s", "\n");

	printf("%d", --n);
	printf("%s", "\n");

	printf("%d", n);
	printf("%s", "\n");

	printf("%d", -n);
	printf("%s", "\n");

	printf("%d", *(&x));
	printf("%s", "\n");

	//printf("%d", ++n);
	//printf("%s", "coucou");
	//printf("%f", 10.0);
	return 2;
}
/*
    | Upre_inc -> code_expr @@ pop a0 @@ lw a1 areg (0, a0) @@ add a1 a1 oi 1 @@ sw a1 areg (0, a0) @@ push a1
    | Upost_inc -> code_expr @@ pop a0 @@ lw a1 areg (0, a0) @@ add a2 a1 oi 1 @@ sw a2 areg (0, a0) @@ push a1
    | Upre_dec -> code_expr @@ pop a0 @@ lw a1 areg (0, a0) @@ sub a1 a1 oi 1 @@ sw a1 areg (0, a0) @@ push a1
    | Upost_dec -> code_expr @@ pop a0 @@ lw a1 areg (0, a0) @@ sub a2 a1 oi 1 @@ sw a2 areg (0, a0) @@ push a1

    | Uminus -> code_expr @@ pop a0 @@ neg a0 a0 @@ push a0
    | Uplus -> code_expr @@ pop a0 @@ abs a0 a0 @@ push a0
    | Unot -> code_expr @@ pop a0 @@ not_ a0 a0 @@ push a0

    */