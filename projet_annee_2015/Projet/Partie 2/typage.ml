open Ast

exception Typage_error of loc * string
module SMap = Map.Make (String)
type env = {local: c_type SMap.t}
let istruct = Hashtbl.create 20
let global = Hashtbl.create 50

let type_constante c =
	match c with
	| Cint _ -> Tint
	| Cdouble _ -> Tdouble
	| Cstring _ -> Tpointer Tchar

let rec type_egal t1 t2 =
	match t1, t2 with
	| Tstruct a, Tstruct b -> a.node = b.node
	| Tpointer ty1, Tpointer ty2 -> type_egal ty1 ty2
	| x, y -> x = y

let max_type t1 t2 loc = 
	if type_egal t1 t2 then 
		t1
	else
	match t1, t2 with
	| Tpointer _, _
	| _, Tpointer _
	| Tvoid, _
	| _, Tvoid
	| Tstruct _, _
	| _, Tstruct _ -> raise (Typage_error (loc, ""))
	| Tnull, t -> t
	| Tchar, t -> t
	| Tint, t -> t
	| Tdouble, t -> Tdouble

let rec test_compatibilite t1 t2 =
	match t1, t2 with 
	| (Tint | Tchar | Tnull | Tdouble), (Tint | Tchar | Tnull | Tdouble)
	| Tnull, Tpointer _
	| Tpointer _, Tnull
	| Tpointer _, Tpointer Tvoid
	| Tpointer Tvoid, Tpointer _ -> true
	| ty1, ty2 -> type_egal ty1 ty2

let rec bien_forme_bis t = 
	match t with
	| Tstruct i -> Hashtbl.mem istruct i.node
	| Tpointer ty -> bien_forme_bis ty
	| _ -> true

let bien_forme_void t id = 
	match t with
	| Tstruct i -> (Hashtbl.mem istruct i.node) && (id.node <> i.node)
	| Tpointer ty -> bien_forme_bis ty
	| Tvoid -> false
	| _ -> true

let rec type_expr env e =
  match e.node with
  | Eassign (e1, e2) -> 
  	let t1 = valeur_gauche env e1 in
  	let t2 = type_expr env e2 in
  	if test_compatibilite t1 t2 then
  		max_type t1 t2 e.loc
  	else
  		raise (Typage_error (e.loc, ""))
  | _ -> raise (Typage_error (e.loc, ""))
and valeur_gauche env e =
  match e.node with
  | Eident i -> 
  	begin 
  		try SMap.find i.node env.local
  		with Not_found -> 
  		begin 
  			try Hashtbl.find global i.node
  			with Not_found -> raise (Typage_error (e.loc, ""))
  		end
  	end
  | Edot (e1, i) ->
  	begin
  		let t1 = valeur_gauche env e1 in
  		match t1 with
  		| Tstruct id ->
  		begin
  			try Hashtbl.find (Hashtbl.find istruct i.node) id.node
	  		with Not_found -> raise (Typage_error (e.loc, ""))
	  	end
  		| _ -> raise (Typage_error (e.loc, ""))
  	end
  | Eunop (u, e1) -> 
  	begin
	  	match u with
	  	| Ustar -> 
	  	begin
	  		let t = type_expr env e1 in
	  			match t with
	  			| Tpointer _ -> t
	  			| _ -> raise (Typage_error (e.loc, ""))
	  	end
	  	| _ -> raise (Typage_error (e.loc, ""))
	end
  | _ -> raise (Typage_error (e.loc, ""))

let rec typage_var_decl vd =
	match vd with
	| [] -> ()
	| (t,i)::s -> 
		if Hashtbl.mem global i.node then 
			raise (Typage_error (i.loc, ""))
		else			
			if bien_forme_void t i then 
				begin
				Hashtbl.add global i.node t;
				typage_var_decl s 
				end
			else 
				raise (Typage_error (i.loc, ""))

let typage_struct_decl (i, vd) =
	if Hashtbl.mem istruct i.node then
		raise (Typage_error (i.loc, ""))
	else
	begin
		let struct_map = Hashtbl.create 5 in
		let champs_map (t, id) =
			if Hashtbl.mem struct_map id.node then
				raise (Typage_error (id.loc, "la variable " ^ id.node ^ " est deja definie"))
			else
				Hashtbl.add struct_map id.node (
				if bien_forme_void t i then 
					t 
				else 
					raise (Typage_error (id.loc, "erreur de declaration (type incorrect ou non defini)")))
		in
		Hashtbl.add istruct i.node struct_map;
		List.iter champs_map vd
	end

let typage_decl d =
	match d with
	| Dvars vd -> typage_var_decl vd
	| Dstruct (id, vd) -> typage_struct_decl (id, vd)
	| _  -> failwith "ok"

let rec typage_file f =
	match f with
	| [] -> ()
	| d::s -> typage_decl d; typage_file s