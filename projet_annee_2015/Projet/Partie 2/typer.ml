open Ast

(* Exception pour les erreurs de typage, renvoyant la location et le message d'erreur correspondant *)
exception Typage_error of loc * string

(* Exception pour la fonction main *)
exception Function_main_error of string

(* ------ Environnements ------ *)

(* Environnement local *)
module SMap = Map.Make (String)
type env = c_type SMap.t

(* Environnement pour les variables globales *)
let global = Hashtbl.create 50

(* Envrionnement pour les fonctions *)
let fonction = Hashtbl.create 10

(* Environnement pour les structures *)
let istruct = Hashtbl.create 20

(* Nom de la fonction malloc *)
let malloc_f = "sbrk"

(* Nom de la fonction printf *)
let printf_f = "printf"


(* ------ Fonctions de test pour les types ------ *)

(* Renvoie vrai si le type est numérique *)
let num t =
	match t with
	| Tstruct _ | Tvoid -> false
	| _ -> true

(* Renvoie vrai si les deux types sont les mêmes. Même nom de structure, même type pointé ou même constructeur) *)
let rec type_egal t1 t2 =
	match t1, t2 with
	| Tstruct a, Tstruct b -> a.node = b.node
	| Tpointer ty1, Tpointer ty2 -> type_egal ty1 ty2
	| x, y -> x = y

let rec afficher_type t = 
	match t with
  | Tnull -> Printf.printf "null"
  | Tvoid -> Printf.printf "void"
  | Tint -> Printf.printf "int"
  | Tchar -> Printf.printf "char"
  | Tstruct ident -> Printf.printf "ident"
  | Tdouble -> Printf.printf "double"
  | Tpointer c_type -> Printf.printf "*"; afficher_type c_type

(* Renvoie le type numérique maximum *)
let max_type t1 t2 loc = 
	if type_egal t1 t2 then 
		t1
	else
		match t1, t2 with
		| Tpointer _, _
		| _, Tpointer _
		| Tvoid, _
		| _, Tvoid
		| Tstruct _, _
		| _, Tstruct _ -> raise (Typage_error (loc, "erreur de type: les types sont incompatibles entre eux"))
		| Tnull, t -> t
		| Tchar, t -> t
		| Tint, t -> t
		| Tdouble, t -> Tdouble

(* Renvoie vrai si les types sont compatibles entre eux *)
let rec test_compatibilite t1 t2 =
	match t1, t2 with 
	| (Tint | Tchar | Tnull | Tdouble), (Tint | Tchar | Tnull | Tdouble)
	| Tnull, Tpointer _
	| Tpointer _, Tnull
	| Tpointer _, Tpointer Tvoid
	| Tpointer Tvoid, Tpointer _ -> true
	| ty1, ty2 -> type_egal ty1 ty2

(* Renvoie vrai si le type est bien formé, c'est-à-dire si les structures ont été déclarés *)
let rec bien_forme t = 
	match t with
	| Tstruct i -> Hashtbl.mem istruct i.node
	| Tpointer ty -> bien_forme ty
	| _ -> true

(* Renvoie vrai si le type est bien forme, en considérant que void est mal formé *)
let bien_forme_void t id = 
	match t with
	| Tstruct i -> (Hashtbl.mem istruct i.node) && (id.node <> i.node)
	| Tpointer ty -> bien_forme ty
	| Tvoid -> false
	| _ -> true

(* Renvoie vrai si le type est compatible pour les opérations de comparaison *)
let type_comparaison t1 =
	match t1 with
	| Tstruct _ | Tvoid -> false
	| _ -> true

(* Renvoie vrai si le type est compatible avec int *)
let type_int t1 =
	match t1 with
	| Tchar | Tint -> true
	| _ -> false

(* Renvoie vrai si le type permet de représenter des string (char* ) *)
let type_string t =
	match t with
	| Tpointer p -> p = Tchar
	| _ -> false


(* ------ Fonctiond de typage ------ *)

(* Renvoie le type de la constante *)
let type_constante c =
	match c with
	| Cint _ -> Tint
	| Cdouble _ -> Tdouble
	| Cstring _ -> Tpointer Tchar

(* Renvoie le type de l'expression e après l'avoir typé avec succès dans l'environnement env *)
let rec typage_expr e env =
  match e.node with
  | Enull -> {loc = (Tnull, false); node = Enull}
  | Econst c -> {loc = type_constante c, false; node = Econst c}
  | Eident i ->
  	begin
	  	try {loc = (SMap.find i.node env, false); node = Eident i}
	  	with Not_found -> 
	  		begin 
	  			try {loc = (Hashtbl.find global i.node, false); node = Eident i}
	  			with Not_found -> raise (Typage_error (e.loc, "erreur de variable: variable non declare"))
	  		end
  	end
  | Esizeof t -> 
  	if (bien_forme t) && (t <> Tvoid) then 
  		{loc = (Tint, false); node = Esizeof t} 
  	else
  		raise (Typage_error (e.loc, "erreur de type: type incorrect ou non defini"))
  | Edot (e1, i) -> 
  	begin
  		let new_expr = typage_expr e1 env in
  		let t1 = fst new_expr.loc in
  		match t1 with
  		| Tstruct id ->
  		begin
  			try {loc = (Hashtbl.find (Hashtbl.find istruct id.node) i.node, false); node = Edot (new_expr, i)}
	  		with Not_found -> raise (Typage_error (e.loc, "erreur de type: structure non definie ou champs inexistant"))
	  	end
  		| _ -> raise (Typage_error (e.loc, "erreur de type: une structure est attendue"))
  	end
  | Eassign (e1, e2) ->
  	let new_expr_1 = valeur_gauche e1 env in
  	let new_expr_2 = typage_expr e2 env in
  	let t1 = fst (new_expr_1).loc in
  	let t2 = fst (new_expr_2).loc in
  	if test_compatibilite t1 t2 then
  		{loc = (t1, false); node = Eassign (new_expr_1, new_expr_2)}
  	else
  		raise (Typage_error (e.loc, "erreur de type: affectation impossible (types non compatibles)"))
  | Eunop (u, e) ->
  	begin
	  	match u with
		| Upre_inc | Upost_inc | Upre_dec | Upost_dec ->
			let new_expr = valeur_gauche e env in
			let t = fst new_expr.loc in
			if num t then
				{loc = (t, false); node = Eunop(u, new_expr)}
			else
				raise (Typage_error (e.loc, "erreur de type: ce type n'est pas numerique"))
		| Ustar ->
			let new_expr = typage_expr e env in
	  		let t = fst new_expr.loc in
	  		begin
	  			match t with
	  			| Tpointer t -> {loc = (t, false); node = Eunop(u, new_expr)}
	  			| _ -> raise (Typage_error (e.loc, "erreur de type: un type pointeur est attendu"))
	  		end
		| Uamp ->
			let new_expr = valeur_gauche e env in
			let t = fst new_expr.loc in
			{loc = (Tpointer t, false); node = Eunop(u, new_expr)}
		| Unot ->
			let new_expr = typage_expr e env in
			let t = fst new_expr.loc in
			if num t then
				{loc = (Tint, false); node = Eunop(u, new_expr)}
			else
				raise (Typage_error (e.loc, "erreur de type: ce type n'est pas numerique"))
		| Uminus | Uplus ->
			let new_expr = typage_expr e env in
			let t = fst new_expr.loc in
			if test_compatibilite t Tdouble then
				{loc = (t, false); node = Eunop(u, new_expr)}
			else
				raise (Typage_error (e.loc, "erreur de type: operation impossible sur ce type"))
	end
  | Ebinop (b, e1, e2) ->
  	begin
	  	match b with
	  	| Beq | Bneq | Blt | Ble | Bgt | Bge ->
	  	begin
	  		let new_expr_1 = typage_expr e1 env in
	  		let t1 = fst new_expr_1.loc in
	  		if type_comparaison t1 then
	  		begin
	  			let new_expr_2 = typage_expr e2 env in
	  			let t2 = fst new_expr_2.loc in
	  			if test_compatibilite t1 t2 then
	  				{loc = (Tint, false); node = Ebinop(b, new_expr_1, new_expr_2)}
	  			else
	  				raise (Typage_error (e2.loc, "erreur de type: type non comparable"))
	  		end
	  		else
	  			raise (Typage_error (e1.loc, "erreur de type: type non comparable"))
	  	end
	  	| Bmul | Bdiv ->
	  	begin
	  		let new_expr_1 = typage_expr e1 env in
	  		let new_expr_2 = typage_expr e2 env in
	  		let t1 = fst new_expr_1.loc in
	  		let t2 = fst new_expr_2.loc in
	  		if test_compatibilite t1 Tdouble then
	  		begin
	  			if test_compatibilite t1 t2 then
	  				{loc = (max_type t1 t2 e.loc, false); node = Ebinop(b, new_expr_1, new_expr_2)}
	  			else
	  				raise (Typage_error (e.loc, "erreur de type: types non compatibles"))
	  		end
	  		else
	  			raise (Typage_error (e.loc, "erreur de type: types non numeriques"))
	  	end
	  	| Badd ->
	  	begin
	  		let new_expr_1 = typage_expr e1 env in
	  		let new_expr_2 = typage_expr e2 env in
	  		let t1 = fst new_expr_1.loc in
	  		let t2 = fst new_expr_2.loc in
	  		(* Arithmétique usuelle *)
	  		if (test_compatibilite t1 Tdouble) && (test_compatibilite t1 t2) then
	  			{loc = (max_type t1 t2 e.loc, false); node = Ebinop(b, new_expr_1, new_expr_2)}
	  		else
	  		(* Arithmétique des pointeurs *)
	  		begin
		  		match t1 with
		  		| Tpointer _ ->
		  			if type_int (max_type t2 Tint e.loc) then
		  				{loc = (t1, false); node = Ebinop(b, new_expr_1, new_expr_2)}
		  			else
		  				raise (Typage_error (e.loc, "erreur de type: le résultat n'est pas numerique")) 
		  		| _ ->
		  		begin
		  			match t2 with
		  			| Tpointer _ ->
		  				if type_int (max_type t1 Tint e.loc) then
		  					{loc = (t2, false); node = Ebinop(b, new_expr_1, new_expr_2)}
		  				else
		  					raise (Typage_error (e.loc, "erreur de type: le resultat n'est pas numerique"))
		  			| _ -> raise (Typage_error (e.loc, "erreur de type: types non compatibles"))
		  		end
	  		end
	  	end
	  	| Bsub ->
	  	begin
	  		let new_expr_1 = typage_expr e1 env in
	  		let new_expr_2 = typage_expr e2 env in
	  		let t1 = fst new_expr_1.loc in
	  		let t2 = fst new_expr_2.loc in
	  		(* Arithmétique usuelle *)
	  		if (test_compatibilite t1 Tdouble) && (test_compatibilite t1 t2) then
	  			{loc = (max_type t1 t2 e.loc, false); node = Ebinop(b, new_expr_1, new_expr_2)}
	  		else 
	  		(* Arithmétique des pointeurs *)
	  		begin
		  		match t1, t2 with
		  		| Tpointer ta, Tpointer tb ->
		  			if type_egal ta tb then
		  				{loc = (Tint, false); node = Ebinop(b, new_expr_1, new_expr_2)}
		  			else
		  				raise (Typage_error (e.loc, "erreur de type: types non compatibles"))
		  		| Tpointer _, _ ->
		  			if type_int (max_type t2 Tint e.loc) then
		  				{loc = (t1, false); node = Ebinop(b, new_expr_1, new_expr_2)}
		  			else
		  				raise (Typage_error (e.loc, "erreur de type: le résultat n'est pas numerique")) 
		  		| _ -> raise (Typage_error (e.loc, "erreur de type: types non compatibles"))
	  		end
	  	end
	  	| Bmod ->
	  		let new_expr_1 = typage_expr e1 env in
	  		let new_expr_2 = typage_expr e2 env in
	  		let t1 = fst new_expr_1.loc in
	  		let t2 = fst new_expr_2.loc in
	  		let tmax = max_type t1 t2 e.loc in
	  		if type_int tmax then
	  			{loc = (tmax, false); node = Ebinop(b, new_expr_1, new_expr_2)}
	  		else
	  			raise (Typage_error (e.loc, "erreur de type: types non entiers"))
	  	| Band | Bor ->
	  		begin
	  		let new_expr_1 = typage_expr e1 env in
	  		let t1 = fst new_expr_1.loc in
	  		if test_compatibilite t1 Tdouble then
	  			begin
	  			let new_expr_2 = typage_expr e2 env in
	  			let t2 = fst new_expr_2.loc in
	  			if test_compatibilite t1 t2 then
	  				{loc = (Tint, false); node = Ebinop(b, new_expr_1, new_expr_2)}
	  			else
	  				raise (Typage_error (e.loc, "erreur de type: types non comparables"))
	  			end
	  		else
	  			raise (Typage_error (e.loc, "erreur de type: types non numeriques"))
	  		end
  	end
  | Ecall (id, expr_list) ->
  begin
  	(* Appel de malloc *)
  	if id.node = malloc_f then
  	begin
  		match expr_list with
  		| expr::[] ->
  		begin
  			let new_expr = typage_expr expr env in
  			let t = fst new_expr.loc in
  			if type_int t then
  				{loc = (Tpointer Tvoid, false); node = Ecall (id, [new_expr])}
  			else
  				raise (Typage_error (e.loc, "erreur de fonction: argument incorrect pour malloc"))
  		end
  		| _ -> raise (Typage_error (e.loc, "erreur de fonction: malloc prend un (unique) argument"))

  	end
  	(* Appel de printf *)
  	else if id.node = printf_f then
 	begin
  		match expr_list with
  		| format::expr::[] ->
  		begin
  			let new_expr = typage_expr expr env in
  			let new_format = typage_expr format env in
  			let t = fst new_expr.loc in
	  		match new_format.node with
	  		| Econst idc ->
	  		begin
		  		match idc with 
		  		| Cstring s ->
		  		begin
		  			if s = "%d" then
		  			begin
		  				if t <> Tint then
		  					raise (Typage_error (expr.loc, "erreur de type: une expression de type int est attendue"))
		  				else
		  					{loc = (Tint, false); node = Ecall (id, new_format::new_expr::[])}
		  			end
		  			else if s = "%f" then
		  			begin
		  				if t <> Tdouble then
		  					raise (Typage_error (expr.loc, "erreur de type: une expression de type float est attendue"))
		  				else
		  					{loc = (Tint, false); node = Ecall (id, new_format::new_expr::[])}
		  			end
		  			else if s = "%s" then
		  			begin
		  				if not (type_string t) then
		  					raise (Typage_error (expr.loc, "erreur de type: une expression de type string est attendue"))
		  				else
		  					{loc = (Tint, false); node = Ecall (id, new_format::new_expr::[])}
		  			end
		  			else
		  				raise (Typage_error (format.loc, "erreur de fonction: format inconnu pour printf"))
		  		end
		  		| _ -> raise (Typage_error (format.loc, "erreur de fonction: un format (string) est attendu"))
		  	end
		  	| _ -> raise (Typage_error (e.loc, "erreur de fonction: printf prend un format de type string"))
  		end
  		| _ -> raise (Typage_error (e.loc, "erreur de fonction: printf prend deux arguments"))
  	end
  	(* Appel de fonctions définies par l'utilisateur *)
  	else
  	try
  		let (type_fonction, type_list) = Hashtbl.find fonction id.node in
  		let rec arg_compatibles expr_list type_list acc loc =
		  	match expr_list, type_list with
			| e::le, t1::lt ->
			let new_expr = typage_expr e env in
		  	let t2 = fst new_expr.loc in
		  	if test_compatibilite t1 t2 then
		  		arg_compatibles le lt (new_expr::acc) loc
		 	 else
		  		raise (Typage_error (loc, "erreur de fonction: arguments non compatibles avec le prototype de la fonction"))
			| [], [] -> acc
			| _, [] -> raise (Typage_error (loc, "erreur de fonction: arguments en trop"))
			| [], _ -> raise (Typage_error (loc, "erreur de fonction: arguments manquant"))
		in
  		let new_exprs = arg_compatibles expr_list type_list [] e.loc in
  		{loc = (type_fonction, false); node = Ecall (id, new_exprs)}
  	with Not_found -> raise (Typage_error (id.loc, "erreur de fonction: fonction non definie"))
  end

(* Renvoie le type de l'expresison gauche *)
and valeur_gauche e env =
  match e.node with
  | Eident i -> 
  	begin 
  		try
  		{loc = (SMap.find i.node env, true); node = Eident i}
  		with Not_found -> 
  		begin 
  			try
  			{loc = (Hashtbl.find global i.node, true); node = Eident i}
  			with Not_found -> raise (Typage_error (e.loc, "erreur de variable: variable non declare"))
  		end
  	end
  | Edot (e1, i) -> 
  	begin
  		let new_expr = valeur_gauche e1 env in 
  		let t1 = fst new_expr.loc in
  		match t1 with
  		| Tstruct id ->
  		begin
  			try
  			{loc = (Hashtbl.find (Hashtbl.find istruct id.node) i.node, true); node = Edot (new_expr, i)}
	  		with Not_found -> raise (Typage_error (e.loc, "erreur de type: structure non definie (" ^ id.node ^ ") ou champs inexistant (" ^ i.node ^ ")"))
	  	end
  		| _ -> raise (Typage_error (e.loc, "erreur de type: une structure est attendue"))
  	end
  | Eunop (u, e1) -> 
  	begin
	  	match u with
	  	| Ustar -> 
	  	begin
	  		let new_expr = typage_expr e1 env in
	  		let t = fst new_expr.loc in
	  			match t with
	  			| Tpointer _ -> {loc = (t, true); node = Eunop (u, new_expr)}
	  			| _ -> raise (Typage_error (e.loc, "erreur de type: un pointeur de type est attendu"))
	  	end
	  	| _ -> raise (Typage_error (e.loc, "erreur de type: type non possible a gauche"))
	end
  | _ -> raise (Typage_error (e.loc, "erreur de type: type non possible a gauche"))

(* Renvoie le nouvel environnement composé des nouvelles variables locales une fois typées avec succès *)
let typage_var_decl_loc vd env =
	let ajoute_var acc (t, id) =
		if SMap.mem id.node acc then 
			raise (Typage_error (id.loc, "erreur de declaration: variable deja declaree"))
		else
			if bien_forme_void t id then 
				SMap.add id.node t acc
			else 
				raise (Typage_error (id.loc, "erreur de declaration: type incorrect"))
	in
	List.fold_left ajoute_var env vd

(* Type l'instruction s dans l'environnement env avec comme type de retour t *)
let rec typage_instruction s env t =
	match s.node with
	| Sskip -> {loc = (t, false); node = Sskip}
	| Sexpr e -> let new_expr = typage_expr e env in {loc = (t, false); node = Sexpr new_expr}
	| Sif (e, i1, i2) ->
		let new_expr = typage_expr e env in
		let type_expr = fst new_expr.loc in
		if num type_expr then
		begin
			let new_instr_1 = typage_instruction i1 env t in
			let new_instr_2 = typage_instruction i2 env t in
			{loc = (t, false); node = Sif(new_expr, new_instr_1, new_instr_2)}
		end
		else
			raise (Typage_error (e.loc, "erreur de condition: l'expression n'est pas numérique"))
	| Swhile (e, i) ->
		let new_expr = typage_expr e env in
		let type_expr = fst new_expr.loc in
		if num type_expr then
			let new_instr = typage_instruction i env t in
			{loc = (t, false); node = Swhile(new_expr, new_instr)}
		else
			raise (Typage_error (e.loc, "erreur de boucle: l'expression n'est pas numérique"))
	| Sfor (li1, e, li2, i) ->
		let new_expr = typage_expr e env in
		let type_expr = fst new_expr.loc in
		if num type_expr then
		begin
			let new_linstr_1 = List.map (fun s -> typage_instruction s env t) li1 in
			let new_linstr_2 = List.map (fun s -> typage_instruction s env t) li2 in
			let new_instr = typage_instruction i env t in
			{loc = (t, false); node = Sfor(new_linstr_1, new_expr, new_linstr_2, new_instr)}
		end
		else
			raise (Typage_error (e.loc, "erreur de boucle: l'expression n'est pas numérique"))
	| Sblock b ->
		{loc = (t, false); node = Sblock (typage_bloc b env t)}
	| Sreturn oexpr_retour ->
	begin
		match oexpr_retour with
		(* Retour d'une expression *)
		| Some e ->
			let new_expr = typage_expr e env in
			let t_retour = fst new_expr.loc in
			if not (test_compatibilite t t_retour) then
				raise (Typage_error (e.loc, "erreur de fonction: type de retour incorrect"))
			else
				{loc = (t, false); node = Sreturn (Some new_expr)}
		(* Retour sans expression *)
		| None ->
			if t <> Tvoid then
				raise (Typage_error (s.loc, "erreur de fonction: type de retour incorrect"))
			else
				{loc = (t, false); node = Sreturn None}
	end

(* Type l'ensemble du bloc *)
and typage_bloc (vd, sl) env t =
	let env = typage_var_decl_loc vd env in
	let new_linstr = List.map (fun s -> (typage_instruction s env t)) sl in
	(vd, new_linstr)

(* Ajoute la variable global à l'environnement global si le typage est un succès *)
let rec typage_var_decl_glob vd =
	match vd with
	| [] -> Dvars vd
	| (t,i)::s -> 
		if Hashtbl.mem global i.node then 
			raise (Typage_error (i.loc, "erreur de variable: la variable a deja ete declaree"))
		else			
			if bien_forme_void t i then 
			begin
				Hashtbl.add global i.node t;
				typage_var_decl_glob s
			end
			else 
				raise (Typage_error (i.loc, "erreur de type: type non declarable"))

(* Ajoute les nouvelles structures à l'environnement des structures après succès du typage *)
let typage_struct_decl (i, vd) =
	if Hashtbl.mem istruct i.node then
		raise (Typage_error (i.loc, "erreur de declaration: structure deja existante"))
	else
	begin
		let struct_map = Hashtbl.create 5 in
		let champs_map (t, id) =
			if Hashtbl.mem struct_map id.node then
				raise (Typage_error (id.loc, "la variable " ^ id.node ^ " est deja definie"))
			else
				Hashtbl.add struct_map id.node (
				if bien_forme_void t i then 
					t 
				else 
					raise (Typage_error (id.loc, "erreur de declaration (type incorrect ou non defini)")))
		in
		Hashtbl.add istruct i.node struct_map;
		List.iter champs_map vd;
		Dstruct (i, vd)
	end

(* Ajoute les nouvelles fonction à l'environnement des fonctions après l'avoir typé avec succès *)
let typage_fonction t id vd b =
	(* Fonctions déjà définies *)
	if (id.node = malloc_f) || (id.node = printf_f) || (Hashtbl.mem fonction id.node) then
		raise (Typage_error (id.loc, "erreur de declaration: fonction deja definie"))
	else if not (bien_forme t) then
		raise (Typage_error (id.loc, "erreur de declaration: type de retour incorrect"))
	else
	begin
		let env = typage_var_decl_loc vd SMap.empty in
		let type_arg_list = List.map (fun (t, _) -> t) vd in
		Hashtbl.add fonction id.node (t, type_arg_list);
		let new_bloc = typage_bloc b env t in
		Dfun (t, id, vd, new_bloc)
	end

(* Procède au typage des différentes déclarations  *)
let typage_decl d =
	match d with
	| Dvars vd -> typage_var_decl_glob vd
	| Dstruct (id, vd) -> typage_struct_decl (id, vd)
	| Dfun (t, id, vd, b) -> typage_fonction t id vd b

(* Permet de passer l'étape du typage du programme *)
let typage_file f lb =
	let new_file = List.map typage_decl f in
	(* Vérification de la fonction main *)
	try
		let (t, arg) = Hashtbl.find fonction "main" in
		if t = Tint then
		begin
			match arg with
			| Tint::Tpointer(Tpointer(Tchar))::[]
			| [] -> new_file
			| _ -> raise (Function_main_error "erreur fonction main: type des arguments incorrect")
		end
		else
			raise (Function_main_error "erreur fonction main: type de retour incorrect")
	with Not_found ->
		raise (Typage_error ((Lexing.lexeme_start_p lb, Lexing.lexeme_end_p lb), "erreur fonction main: fonction main non definie"))