

struct coord {
	int x, y;
	char d;
	struct coord* z;
};

int max(int a, int b) {
	int x;
	if(a > b)
		return a;
	else
		return b;
}

int toto(int i, double f, char c) {
	for(i = 0; i < 50; i++) {
		if(i%2 == 0)
			return i;
	}
}

//#include <stdio.h>

int main(int argc, char** argv) {
	int x;
	int y;
	int b;
	int *p;
	struct coord c;
	x = 10;
	argc = 50;
	y = 6666;
	x = y;
	argc = x;
	x = sizeof(int);
	y = sizeof(char);
	argc = sizeof(double);
	x = sizeof(char*);
	x = sizeof(struct coord);
	//x = sizeof(struct toto);
	x++;
	++x;
	y--;
	--y;
	p = &y;
	x = !y;
	+x;
	-y;
	c.x = 50;
	c.y = argc;
	c.d = 'p';
	//c.d = "aaa";
	//c.z = 0;
	!x;
	&p;
	//*c;
	*c.z;
	b == 5;
	'a' == 'b';
	10 == 'r';
	10;
	5.3 == 10;
	'a' == 9.4; // ????
	10 == ( 5% 3 == 2);
	//argc != (5.0%2);
	((c.x || c.y ) && (1 || 0)) <= (10%2 == (50 / 'a'));
	//main(10);
	//x = malloc(10);
	//c.z = sbrk(10); // ??????????
	printf("%d", 10);
	printf("%d", 'o');
	//printf("%d", 5.0);
	//printf("%d", "toto");
	//printf("%f", 10);
	//printf("%f", 'o');
	printf("%f", 5.0);
	//printf("%f", "toto");
	//printf("%s", 10);
	//printf("%s", 'o');
	//printf("%s", 5.0);
	printf("%s", "toto");
	argc = max(5, 10);
	toto(5, 5.0, 'b');
	//toto('d', b, p);
	printf("%d", 'b');
	//return 0;
}

/*

  | Enull (* Insérée automatiquement à partir de 0 *)
  | Econst of constant
  | Eident of ident
  | Esizeof of c_type
  | Edot of 'info expr * ident
  | Eassign of 'info expr * 'info expr
  | Eunop of unop * 'info expr
  | Ebinop of binop * 'info expr * 'info expr
  | Ecall of ident * 'info expr list

  */
