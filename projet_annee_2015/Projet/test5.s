.text
main:
	move $a0, $sp
	rem $a0, $a0, 8
	sub $sp, $sp, $a0
	jal fun_main
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 17
	syscall
fun_max:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	lw $a0, 16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sge $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_0
	lw $a0, 16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a0, 24($fp)
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_max_return
	b suite_0
branch_0:
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a0, 24($fp)
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_max_return
suite_0:
#desalloc varBlock
	add $sp, $sp, 0
fun_max_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_factorielle:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sle $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_1
	li $a0, 1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a0, 16($fp)
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_factorielle_return
	b suite_1
branch_1:
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
#alloc_res
	sub $sp, $sp, 8
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sub $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_factorielle
#dessaloc_arg
	add $sp, $sp, 8
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	mul $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a0, 16($fp)
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_factorielle_return
suite_1:
#desalloc varBlock
	add $sp, $sp, 0
fun_factorielle_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_initCoord:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 8
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, 4($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sub $sp, $sp, 8
	lw $a0, -24($fp)
	sw $a0, 0($sp)
	lw $a0, -20($fp)
	sw $a0, 4($sp)
	lw $a0, 0($sp)
	sw $a0, 24($fp)
	lw $a0, 4($sp)
	sw $a0, 28($fp)
	add $sp, $sp, 8
#beginRet
	add $sp, $sp, 8
#endRet
	b fun_initCoord_return
#desalloc varBlock
	add $sp, $sp, 8
fun_initCoord_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_initDefaultCoord:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
#alloc_res
	sub $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	jal fun_initCoord
#dessaloc_arg
	add $sp, $sp, 16
	lw $a0, 0($sp)
	sw $a0, 8($fp)
	lw $a0, 4($sp)
	sw $a0, 12($fp)
	add $sp, $sp, 8
#beginRet
	add $sp, $sp, 0
#endRet
	b fun_initDefaultCoord_return
#desalloc varBlock
	add $sp, $sp, 0
fun_initDefaultCoord_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_afficherCoord:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 16
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, 4($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_3
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_4
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_5
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 16
fun_afficherCoord_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_println:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	la $a0, str_5
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 0
fun_println_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_printStr:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 0
fun_printStr_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_printInt:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	lw $a0, 8($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 0
fun_printInt_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_printDbl:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 0
	ldc1 $f0, 8($fp)
	sub $sp, $sp, 8
	s.d $f0 0($sp)
	l.d $f12 0($sp)
	add $sp, $sp, 8
	li $v0, 3
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 0
fun_printDbl_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_po:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 8
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, 4($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 50
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sub $sp, $sp, 8
	lw $a0, -20($fp)
	sw $a0, 4($sp)
	lw $a0, 4($sp)
	sw $a0, 12($fp)
	add $sp, $sp, 8
#beginRet
	add $sp, $sp, 8
#endRet
	b fun_po_return
#desalloc varBlock
	add $sp, $sp, 8
fun_po_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
fun_main:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 32
	la $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
#alloc_res
	sub $sp, $sp, 8
	li $a0, 1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 5
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_max
#dessaloc_arg
	add $sp, $sp, 16
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, glob_x
	sub $sp, $sp, 8
	sw $a0, 0($sp)
#alloc_res
	sub $sp, $sp, 8
	lw $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_factorielle
#dessaloc_arg
	add $sp, $sp, 8
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, glob_x
	sub $sp, $sp, 8
	sw $a0, 0($sp)
#alloc_res
	sub $sp, $sp, 8
	li $a0, 10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 20
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_max
#dessaloc_arg
	add $sp, $sp, 16
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	lw $a0, glob_x
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_printInt
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	jal fun_println
#dessaloc_arg
	add $sp, $sp, 0
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
#alloc_res
	sub $sp, $sp, 8
	li $a0, 10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 40
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_max
#dessaloc_arg
	add $sp, $sp, 16
	jal fun_printInt
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	jal fun_println
#dessaloc_arg
	add $sp, $sp, 0
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	lw $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_printInt
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	jal fun_println
#dessaloc_arg
	add $sp, $sp, 0
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	lw $a0, glob_x
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_printInt
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	jal fun_println
#dessaloc_arg
	add $sp, $sp, 0
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 8
	jal fun_po
#dessaloc_arg
	add $sp, $sp, 0
	la $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a1, 4($sp)
	sw $a1, -4($a0)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	la $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, 4($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_printInt
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	jal fun_println
#dessaloc_arg
	add $sp, $sp, 0
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 8
	jal fun_initDefaultCoord
#dessaloc_arg
	add $sp, $sp, 0
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a1, 0($sp)
	sw $a1, -8($a0)
	lw $a1, 4($sp)
	sw $a1, -4($a0)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 8
	li $a0, 5
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	jal fun_initCoord
#dessaloc_arg
	add $sp, $sp, 16
	la $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a1, 0($sp)
	sw $a1, -8($a0)
	lw $a1, 4($sp)
	sw $a1, -4($a0)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	sub $sp, $sp, 8
	lw $a0, -24($fp)
	sw $a0, 0($sp)
	lw $a0, -20($fp)
	sw $a0, 4($sp)
	jal fun_afficherCoord
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#alloc_res
	sub $sp, $sp, 0
	sub $sp, $sp, 8
	lw $a0, -32($fp)
	sw $a0, 0($sp)
	lw $a0, -28($fp)
	sw $a0, 4($sp)
	jal fun_afficherCoord
#dessaloc_arg
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a0, 8($fp)
#beginRet
	add $sp, $sp, 32
#endRet
	b fun_main_return
#desalloc varBlock
	add $sp, $sp, 32
fun_main_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
.data
float_0.:
	.double 0.000000
str_5:
	.asciiz "\n"
str_4:
	.asciiz "y = "
str_3:
	.asciiz ", "
str_2:
	.asciiz "x = "
glob_x:
	.word 0
