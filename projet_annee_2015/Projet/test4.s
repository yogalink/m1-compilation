.text
main:
	move $a0, $sp
	rem $a0, $a0, 8
	sub $sp, $sp, $a0
	jal fun_main
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 17
	syscall
fun_main:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 8
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
branch_0:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sle $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, suite_0
#alloc varBlock
	sub $sp, $sp, 0
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a1, 0($a0)
	add $a2, $a1, 1
	sw $a2, 0($a0)
	sub $sp, $sp, 8
	sw $a1, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 0
	b branch_0
suite_0:
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 20
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
branch_2:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sgt $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, suite_2
#alloc varBlock
	sub $sp, $sp, 0
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 0
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a1, 0($a0)
	sub $a2, $a1, 1
	sw $a2, 0($a0)
	sub $sp, $sp, 8
	sw $a1, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b branch_2
suite_2:
#desalloc varBlock
	add $sp, $sp, 8
fun_main_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
.data
float_0.:
	.double 0.000000
str_1:
	.asciiz "\n"
