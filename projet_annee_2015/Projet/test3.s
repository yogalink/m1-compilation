.text
main:
	move $a0, $sp
	rem $a0, $a0, 8
	sub $sp, $sp, $a0
	jal fun_main
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 17
	syscall
fun_main:
	sub $sp, $sp, 8
	sw $fp, 0($sp)
	move $fp, $sp
	sub $sp, $sp, 8
	sw $ra, 0($sp)
#alloc varBlock
	sub $sp, $sp, 72
	la $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 3
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	seq $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_0
	la $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_2
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_0
branch_0:
	la $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_0:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sne $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_3
	la $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_5
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_3
branch_3:
	la $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_4
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_3:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	slt $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_6
	la $a0, -48($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_8
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_6
branch_6:
	la $a0, -48($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_7
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_6:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sle $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_9
	la $a0, -56($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_11
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_9
branch_9:
	la $a0, -56($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_10
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_9:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sgt $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_12
	la $a0, -64($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_14
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_12
branch_12:
	la $a0, -64($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_13
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_12:
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sge $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, branch_15
	la $a0, -72($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_17
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	b suite_15
branch_15:
	la $a0, -72($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	la $a0, str_16
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
suite_15:
	lw $a0, -32($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -40($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -48($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -56($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -64($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -72($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	add $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sub $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	mul $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	div $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	rem $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	seq $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, lazy_19
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 20
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	seq $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	beqz $a0, lazy_19
	li $a0, 1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	b lazyDone_19
lazy_19:
	li $a0, 0
	sub $sp, $sp, 8
	sw $a0, 0($sp)
lazyDone_19:
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, -16($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	seq $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	bnez $a0, lazy_20
	lw $a0, -24($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	li $a0, 20
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	seq $a0, $a0, $a1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	bnez $a0, lazy_20
	li $a0, 0
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	b lazyDone_20
lazy_20:
	li $a0, 1
	sub $sp, $sp, 8
	sw $a0, 0($sp)
lazyDone_20:
	lw $a1, 0($sp)
	add $sp, $sp, 8
	lw $a0, 0($sp)
	add $sp, $sp, 8
	sw $a1, 0($a0)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	lw $a0, -80($fp)
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 1
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	la $a0, str_18
	sub $sp, $sp, 8
	sw $a0, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
	li $v0, 4
	syscall
	sub $sp, $sp, 8
	sw $zero, 0($sp)
	lw $a0, 0($sp)
	add $sp, $sp, 8
#desalloc varBlock
	add $sp, $sp, 72
fun_main_return:
	lw $ra, 0($sp)
	add $sp, $sp, 8
	lw $fp, 0($sp)
	add $sp, $sp, 8
	jr $ra
.data
float_0.:
	.double 0.000000
str_1:
	.asciiz "egal: faux"
str_13:
	.asciiz "sup: faux"
str_11:
	.asciiz "infeq: vrai"
str_18:
	.asciiz "\n"
str_7:
	.asciiz "inf: faux"
str_2:
	.asciiz "egal: vrai"
str_10:
	.asciiz "infeq: faux"
str_4:
	.asciiz "different: faux"
str_17:
	.asciiz "supeq: vrai"
str_16:
	.asciiz "supeq: faux"
str_8:
	.asciiz "inf: vrai"
str_14:
	.asciiz "sup: vrai"
str_5:
	.asciiz "different: vrai"
