type token =
  | IDENT of (string)
  | CINT of (int32)
  | CDOUBLE of (float)
  | CSTR of (string)
  | FOR
  | IF
  | ELSE
  | WHILE
  | RETURN
  | SIZEOF
  | VOID
  | INT
  | CHAR
  | STRUCT
  | DOUBLE
  | LPAR
  | RPAR
  | LBRACE
  | RBRACE
  | LSQUARE
  | RSQUARE
  | SEMICOLON
  | COMMA
  | DOT
  | ARROW
  | EOF
  | EQ
  | OR
  | AND
  | EQOP of (Ast.binop)
  | COMP of (Ast.binop)
  | PLUS
  | MINUS
  | STAR
  | SLASH
  | PERCENT
  | PLUSPLUS
  | MINUSMINUS
  | BANG
  | AMPERSAND

open Parsing;;
let _ = parse_error;;
# 4 "parser.mly"
  open Ast

  (* déclarateurs:
     représentation intermédiaire permettant de convertire par ex:
     int x, **y, z[10];
     en (int, x); (int**, y), (int[10], z)
  *)

  let mk_loc e l = { loc = l; node = e }

  let loc e =
    mk_loc e (Parsing.symbol_start_pos (),Parsing.symbol_end_pos())

  let loc_i i e =
    mk_loc e (Parsing.rhs_start_pos i, Parsing.rhs_end_pos i)

  let loc_dummy e =
    mk_loc e (Lexing.dummy_pos, Lexing.dummy_pos)


  type declarator =
    | Dident of ident
    | Dpointer of declarator

  let rec declarator ty = function
    | Dident id -> ty, id
    | Dpointer d -> declarator (Tpointer ty) d

# 75 "parser.ml"
let yytransl_const = [|
  261 (* FOR *);
  262 (* IF *);
  263 (* ELSE *);
  264 (* WHILE *);
  265 (* RETURN *);
  266 (* SIZEOF *);
  267 (* VOID *);
  268 (* INT *);
  269 (* CHAR *);
  270 (* STRUCT *);
  271 (* DOUBLE *);
  272 (* LPAR *);
  273 (* RPAR *);
  274 (* LBRACE *);
  275 (* RBRACE *);
  276 (* LSQUARE *);
  277 (* RSQUARE *);
  278 (* SEMICOLON *);
  279 (* COMMA *);
  280 (* DOT *);
  281 (* ARROW *);
    0 (* EOF *);
  282 (* EQ *);
  283 (* OR *);
  284 (* AND *);
  287 (* PLUS *);
  288 (* MINUS *);
  289 (* STAR *);
  290 (* SLASH *);
  291 (* PERCENT *);
  292 (* PLUSPLUS *);
  293 (* MINUSMINUS *);
  294 (* BANG *);
  295 (* AMPERSAND *);
    0|]

let yytransl_block = [|
  257 (* IDENT *);
  258 (* CINT *);
  259 (* CDOUBLE *);
  260 (* CSTR *);
  285 (* EQOP *);
  286 (* COMP *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\003\000\003\000\003\000\007\000\006\000\
\006\000\004\000\008\000\008\000\013\000\013\000\010\000\010\000\
\010\000\010\000\010\000\014\000\014\000\012\000\012\000\011\000\
\011\000\015\000\015\000\015\000\015\000\015\000\015\000\015\000\
\015\000\015\000\015\000\015\000\015\000\015\000\015\000\015\000\
\015\000\015\000\015\000\015\000\015\000\015\000\015\000\015\000\
\015\000\015\000\015\000\015\000\015\000\015\000\017\000\017\000\
\017\000\017\000\017\000\017\000\017\000\017\000\017\000\018\000\
\018\000\016\000\016\000\019\000\019\000\020\000\020\000\009\000\
\005\000\000\000"

let yylen = "\002\000\
\002\000\000\000\002\000\001\000\006\000\005\000\002\000\000\000\
\002\000\003\000\000\000\001\000\002\000\004\000\001\000\001\000\
\001\000\001\000\002\000\001\000\002\000\001\000\003\000\001\000\
\002\000\003\000\002\000\002\000\002\000\002\000\003\000\003\000\
\003\000\003\000\003\000\003\000\003\000\003\000\003\000\004\000\
\002\000\002\000\002\000\002\000\001\000\001\000\001\000\004\000\
\001\000\002\000\004\000\003\000\003\000\003\000\001\000\002\000\
\005\000\007\000\009\000\005\000\001\000\002\000\003\000\000\000\
\001\000\000\000\001\000\001\000\003\000\000\000\002\000\004\000\
\001\000\002\000"

let yydefred = "\000\000\
\002\000\000\000\074\000\000\000\015\000\016\000\017\000\000\000\
\018\000\001\000\003\000\004\000\000\000\000\000\073\000\000\000\
\000\000\000\000\024\000\000\000\000\000\000\000\000\000\000\000\
\000\000\012\000\025\000\000\000\010\000\000\000\000\000\000\000\
\019\000\000\000\000\000\000\000\023\000\009\000\000\000\000\000\
\006\000\000\000\005\000\000\000\014\000\046\000\045\000\047\000\
\000\000\000\000\000\000\000\000\000\000\000\000\055\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\061\000\
\000\000\000\000\000\000\000\000\000\000\000\000\062\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\056\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\029\000\030\000\071\000\072\000\000\000\000\000\067\000\000\000\
\000\000\063\000\020\000\000\000\054\000\000\000\000\000\052\000\
\053\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\040\000\
\021\000\048\000\051\000\069\000\000\000\000\000\000\000\060\000\
\000\000\000\000\000\000\058\000\000\000\059\000"

let yydgoto = "\002\000\
\003\000\004\000\011\000\030\000\063\000\031\000\013\000\024\000\
\064\000\032\000\036\000\021\000\026\000\108\000\065\000\102\000\
\066\000\134\000\103\000\067\000"

let yysindex = "\019\000\
\000\000\000\000\000\000\001\000\000\000\000\000\000\000\020\255\
\000\000\000\000\000\000\000\000\018\255\013\255\000\000\011\255\
\137\255\013\255\000\000\025\255\028\255\137\255\020\255\037\255\
\013\255\000\000\000\000\013\255\000\000\137\255\038\255\013\255\
\000\000\041\255\033\255\025\255\000\000\000\000\039\255\137\255\
\000\000\137\255\000\000\162\255\000\000\000\000\000\000\000\000\
\047\255\050\255\052\255\186\255\054\255\210\255\000\000\210\255\
\210\255\210\255\210\255\210\255\210\255\210\255\055\255\000\000\
\230\255\162\255\065\255\210\255\210\255\210\255\000\000\017\001\
\137\255\023\000\110\255\110\255\110\255\110\255\110\255\110\255\
\110\255\210\255\210\255\000\000\020\255\020\255\210\255\210\255\
\210\255\210\255\210\255\210\255\210\255\210\255\210\255\210\255\
\000\000\000\000\000\000\000\000\035\001\064\255\000\000\044\000\
\065\000\000\000\000\000\251\254\000\000\070\255\053\001\000\000\
\000\000\071\001\089\001\107\001\083\255\125\001\143\001\143\001\
\110\255\110\255\110\255\210\255\210\255\162\255\162\255\000\000\
\000\000\000\000\000\000\000\000\071\001\066\255\087\255\000\000\
\210\255\162\255\079\255\000\000\162\255\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\014\255\
\084\255\000\000\000\000\003\255\000\000\071\255\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\073\255\000\000\000\000\
\000\000\000\000\085\255\078\255\000\000\000\000\000\000\073\255\
\000\000\000\000\000\000\102\255\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\002\000\000\000\
\000\000\102\255\000\000\114\255\000\000\000\000\000\000\000\000\
\000\000\000\000\086\000\105\000\124\000\143\000\162\000\181\000\
\200\000\120\255\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\010\255\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\076\255\148\001\210\001\211\255\196\001\164\001\180\001\
\219\000\238\000\001\001\000\000\116\255\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\118\255\000\000\123\255\000\000\
\120\255\000\000\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\000\000\000\000\000\000\139\000\037\000\239\255\000\000\000\000\
\110\000\007\000\008\000\125\000\115\000\000\000\204\255\177\255\
\182\255\000\000\034\000\103\000"

let yytablesize = 750
let yytable = "\072\000\
\010\000\074\000\110\000\075\000\076\000\077\000\078\000\079\000\
\080\000\081\000\014\000\128\000\038\000\015\000\019\000\101\000\
\104\000\105\000\007\000\001\000\015\000\020\000\044\000\025\000\
\022\000\027\000\068\000\129\000\022\000\101\000\111\000\068\000\
\035\000\017\000\114\000\115\000\116\000\117\000\118\000\119\000\
\120\000\121\000\122\000\123\000\016\000\018\000\019\000\028\000\
\025\000\029\000\019\000\135\000\136\000\034\000\019\000\042\000\
\039\000\139\000\040\000\033\000\043\000\019\000\068\000\140\000\
\019\000\069\000\142\000\070\000\019\000\073\000\082\000\101\000\
\133\000\008\000\008\000\008\000\008\000\008\000\008\000\107\000\
\008\000\008\000\008\000\100\000\101\000\125\000\130\000\137\000\
\008\000\008\000\008\000\008\000\026\000\138\000\008\000\141\000\
\026\000\026\000\026\000\022\000\011\000\013\000\083\000\008\000\
\008\000\008\000\085\000\086\000\008\000\008\000\008\000\008\000\
\091\000\092\000\093\000\094\000\095\000\096\000\097\000\098\000\
\070\000\112\000\113\000\057\000\057\000\057\000\057\000\057\000\
\057\000\083\000\057\000\057\000\057\000\085\000\086\000\066\000\
\066\000\064\000\057\000\065\000\057\000\057\000\012\000\041\000\
\057\000\097\000\098\000\005\000\006\000\007\000\023\000\009\000\
\037\000\057\000\057\000\057\000\045\000\132\000\057\000\057\000\
\057\000\057\000\015\000\046\000\047\000\048\000\049\000\050\000\
\099\000\051\000\052\000\053\000\000\000\000\000\000\000\000\000\
\000\000\054\000\000\000\040\000\000\000\000\000\000\000\055\000\
\000\000\000\000\015\000\046\000\047\000\048\000\000\000\000\000\
\056\000\057\000\058\000\053\000\000\000\059\000\060\000\061\000\
\062\000\054\000\000\000\000\000\000\000\000\000\000\000\071\000\
\000\000\000\000\015\000\046\000\047\000\048\000\000\000\000\000\
\056\000\057\000\058\000\053\000\000\000\059\000\060\000\061\000\
\062\000\054\000\000\000\033\000\000\000\000\000\000\000\033\000\
\033\000\033\000\000\000\000\000\033\000\033\000\033\000\033\000\
\056\000\057\000\058\000\000\000\000\000\059\000\060\000\061\000\
\062\000\083\000\000\000\084\000\000\000\085\000\086\000\087\000\
\088\000\089\000\090\000\091\000\092\000\093\000\094\000\095\000\
\096\000\097\000\098\000\005\000\006\000\007\000\008\000\009\000\
\000\000\000\000\049\000\000\000\000\000\049\000\049\000\049\000\
\049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
\049\000\049\000\049\000\049\000\049\000\049\000\049\000\109\000\
\000\000\000\000\083\000\000\000\000\000\000\000\085\000\086\000\
\087\000\088\000\089\000\090\000\091\000\092\000\093\000\094\000\
\095\000\096\000\097\000\098\000\126\000\000\000\000\000\083\000\
\000\000\000\000\000\000\085\000\086\000\087\000\088\000\089\000\
\090\000\091\000\092\000\093\000\094\000\095\000\096\000\097\000\
\098\000\127\000\000\000\000\000\083\000\000\000\000\000\000\000\
\085\000\086\000\087\000\088\000\089\000\090\000\091\000\092\000\
\093\000\094\000\095\000\096\000\097\000\098\000\041\000\000\000\
\000\000\000\000\041\000\041\000\041\000\000\000\000\000\041\000\
\041\000\041\000\041\000\041\000\041\000\041\000\041\000\041\000\
\041\000\042\000\000\000\000\000\000\000\042\000\042\000\042\000\
\000\000\000\000\042\000\042\000\042\000\042\000\042\000\042\000\
\042\000\042\000\042\000\042\000\050\000\000\000\000\000\000\000\
\050\000\050\000\050\000\000\000\000\000\050\000\050\000\050\000\
\050\000\050\000\050\000\050\000\050\000\050\000\050\000\028\000\
\000\000\000\000\000\000\028\000\028\000\028\000\000\000\000\000\
\028\000\028\000\028\000\028\000\028\000\028\000\028\000\028\000\
\028\000\028\000\027\000\000\000\000\000\000\000\027\000\027\000\
\027\000\000\000\000\000\027\000\027\000\027\000\027\000\027\000\
\027\000\027\000\027\000\027\000\027\000\043\000\000\000\000\000\
\000\000\043\000\043\000\043\000\000\000\000\000\043\000\043\000\
\043\000\043\000\043\000\043\000\043\000\043\000\043\000\043\000\
\044\000\000\000\000\000\000\000\044\000\044\000\044\000\000\000\
\000\000\044\000\044\000\044\000\044\000\044\000\044\000\044\000\
\044\000\044\000\044\000\037\000\000\000\000\000\000\000\037\000\
\037\000\037\000\000\000\000\000\037\000\037\000\037\000\037\000\
\037\000\037\000\037\000\037\000\037\000\037\000\038\000\000\000\
\000\000\000\000\038\000\038\000\038\000\000\000\000\000\038\000\
\038\000\038\000\038\000\038\000\038\000\038\000\038\000\038\000\
\038\000\039\000\000\000\000\000\000\000\039\000\039\000\039\000\
\000\000\000\000\039\000\039\000\039\000\039\000\039\000\039\000\
\039\000\039\000\039\000\039\000\083\000\000\000\106\000\000\000\
\085\000\086\000\087\000\088\000\089\000\090\000\091\000\092\000\
\093\000\094\000\095\000\096\000\097\000\098\000\083\000\000\000\
\000\000\124\000\085\000\086\000\087\000\088\000\089\000\090\000\
\091\000\092\000\093\000\094\000\095\000\096\000\097\000\098\000\
\083\000\131\000\000\000\000\000\085\000\086\000\087\000\088\000\
\089\000\090\000\091\000\092\000\093\000\094\000\095\000\096\000\
\097\000\098\000\083\000\000\000\000\000\000\000\085\000\086\000\
\087\000\088\000\089\000\090\000\091\000\092\000\093\000\094\000\
\095\000\096\000\097\000\098\000\083\000\000\000\000\000\000\000\
\085\000\086\000\000\000\000\000\089\000\090\000\091\000\092\000\
\093\000\094\000\095\000\096\000\097\000\098\000\083\000\000\000\
\000\000\000\000\085\000\086\000\000\000\000\000\000\000\090\000\
\091\000\092\000\093\000\094\000\095\000\096\000\097\000\098\000\
\083\000\000\000\000\000\000\000\085\000\086\000\000\000\000\000\
\000\000\000\000\000\000\092\000\093\000\094\000\095\000\096\000\
\097\000\098\000\083\000\000\000\031\000\000\000\085\000\086\000\
\031\000\031\000\031\000\000\000\000\000\031\000\031\000\094\000\
\095\000\096\000\097\000\098\000\035\000\000\000\000\000\000\000\
\035\000\035\000\035\000\000\000\000\000\035\000\035\000\035\000\
\035\000\035\000\035\000\035\000\036\000\000\000\000\000\000\000\
\036\000\036\000\036\000\000\000\000\000\036\000\036\000\036\000\
\036\000\036\000\036\000\036\000\034\000\000\000\000\000\000\000\
\034\000\034\000\034\000\000\000\000\000\034\000\034\000\034\000\
\034\000\034\000\032\000\000\000\000\000\000\000\032\000\032\000\
\032\000\000\000\000\000\032\000\032\000\032\000"

let yycheck = "\052\000\
\000\000\054\000\082\000\056\000\057\000\058\000\059\000\060\000\
\061\000\062\000\004\000\017\001\030\000\001\001\001\001\068\000\
\069\000\070\000\016\001\001\000\001\001\014\000\040\000\017\000\
\022\001\018\000\017\001\033\001\018\001\082\000\083\000\022\001\
\025\000\016\001\087\000\088\000\089\000\090\000\091\000\092\000\
\093\000\094\000\095\000\096\000\008\000\033\001\033\001\023\001\
\042\000\022\001\014\000\126\000\127\000\017\001\018\000\023\001\
\019\001\137\000\018\001\023\000\022\001\025\000\016\001\138\000\
\028\000\016\001\141\000\016\001\032\000\016\001\016\001\124\000\
\125\000\001\001\002\001\003\001\004\001\005\001\006\001\073\000\
\008\001\009\001\010\001\019\001\137\000\022\001\017\001\022\001\
\016\001\019\001\018\001\019\001\017\001\007\001\022\001\017\001\
\021\001\022\001\023\001\022\001\017\001\017\001\020\001\031\001\
\032\001\033\001\024\001\025\001\036\001\037\001\038\001\039\001\
\030\001\031\001\032\001\033\001\034\001\035\001\036\001\037\001\
\019\001\085\000\086\000\001\001\002\001\003\001\004\001\005\001\
\006\001\020\001\008\001\009\001\010\001\024\001\025\001\022\001\
\017\001\022\001\016\001\022\001\018\001\019\001\004\000\034\000\
\022\001\036\001\037\001\011\001\012\001\013\001\014\001\015\001\
\028\000\031\001\032\001\033\001\042\000\124\000\036\001\037\001\
\038\001\039\001\001\001\002\001\003\001\004\001\005\001\006\001\
\066\000\008\001\009\001\010\001\255\255\255\255\255\255\255\255\
\255\255\016\001\255\255\018\001\255\255\255\255\255\255\022\001\
\255\255\255\255\001\001\002\001\003\001\004\001\255\255\255\255\
\031\001\032\001\033\001\010\001\255\255\036\001\037\001\038\001\
\039\001\016\001\255\255\255\255\255\255\255\255\255\255\022\001\
\255\255\255\255\001\001\002\001\003\001\004\001\255\255\255\255\
\031\001\032\001\033\001\010\001\255\255\036\001\037\001\038\001\
\039\001\016\001\255\255\017\001\255\255\255\255\255\255\021\001\
\022\001\023\001\255\255\255\255\026\001\027\001\028\001\029\001\
\031\001\032\001\033\001\255\255\255\255\036\001\037\001\038\001\
\039\001\020\001\255\255\022\001\255\255\024\001\025\001\026\001\
\027\001\028\001\029\001\030\001\031\001\032\001\033\001\034\001\
\035\001\036\001\037\001\011\001\012\001\013\001\014\001\015\001\
\255\255\255\255\017\001\255\255\255\255\020\001\021\001\022\001\
\023\001\024\001\025\001\026\001\027\001\028\001\029\001\030\001\
\031\001\032\001\033\001\034\001\035\001\036\001\037\001\017\001\
\255\255\255\255\020\001\255\255\255\255\255\255\024\001\025\001\
\026\001\027\001\028\001\029\001\030\001\031\001\032\001\033\001\
\034\001\035\001\036\001\037\001\017\001\255\255\255\255\020\001\
\255\255\255\255\255\255\024\001\025\001\026\001\027\001\028\001\
\029\001\030\001\031\001\032\001\033\001\034\001\035\001\036\001\
\037\001\017\001\255\255\255\255\020\001\255\255\255\255\255\255\
\024\001\025\001\026\001\027\001\028\001\029\001\030\001\031\001\
\032\001\033\001\034\001\035\001\036\001\037\001\017\001\255\255\
\255\255\255\255\021\001\022\001\023\001\255\255\255\255\026\001\
\027\001\028\001\029\001\030\001\031\001\032\001\033\001\034\001\
\035\001\017\001\255\255\255\255\255\255\021\001\022\001\023\001\
\255\255\255\255\026\001\027\001\028\001\029\001\030\001\031\001\
\032\001\033\001\034\001\035\001\017\001\255\255\255\255\255\255\
\021\001\022\001\023\001\255\255\255\255\026\001\027\001\028\001\
\029\001\030\001\031\001\032\001\033\001\034\001\035\001\017\001\
\255\255\255\255\255\255\021\001\022\001\023\001\255\255\255\255\
\026\001\027\001\028\001\029\001\030\001\031\001\032\001\033\001\
\034\001\035\001\017\001\255\255\255\255\255\255\021\001\022\001\
\023\001\255\255\255\255\026\001\027\001\028\001\029\001\030\001\
\031\001\032\001\033\001\034\001\035\001\017\001\255\255\255\255\
\255\255\021\001\022\001\023\001\255\255\255\255\026\001\027\001\
\028\001\029\001\030\001\031\001\032\001\033\001\034\001\035\001\
\017\001\255\255\255\255\255\255\021\001\022\001\023\001\255\255\
\255\255\026\001\027\001\028\001\029\001\030\001\031\001\032\001\
\033\001\034\001\035\001\017\001\255\255\255\255\255\255\021\001\
\022\001\023\001\255\255\255\255\026\001\027\001\028\001\029\001\
\030\001\031\001\032\001\033\001\034\001\035\001\017\001\255\255\
\255\255\255\255\021\001\022\001\023\001\255\255\255\255\026\001\
\027\001\028\001\029\001\030\001\031\001\032\001\033\001\034\001\
\035\001\017\001\255\255\255\255\255\255\021\001\022\001\023\001\
\255\255\255\255\026\001\027\001\028\001\029\001\030\001\031\001\
\032\001\033\001\034\001\035\001\020\001\255\255\022\001\255\255\
\024\001\025\001\026\001\027\001\028\001\029\001\030\001\031\001\
\032\001\033\001\034\001\035\001\036\001\037\001\020\001\255\255\
\255\255\023\001\024\001\025\001\026\001\027\001\028\001\029\001\
\030\001\031\001\032\001\033\001\034\001\035\001\036\001\037\001\
\020\001\021\001\255\255\255\255\024\001\025\001\026\001\027\001\
\028\001\029\001\030\001\031\001\032\001\033\001\034\001\035\001\
\036\001\037\001\020\001\255\255\255\255\255\255\024\001\025\001\
\026\001\027\001\028\001\029\001\030\001\031\001\032\001\033\001\
\034\001\035\001\036\001\037\001\020\001\255\255\255\255\255\255\
\024\001\025\001\255\255\255\255\028\001\029\001\030\001\031\001\
\032\001\033\001\034\001\035\001\036\001\037\001\020\001\255\255\
\255\255\255\255\024\001\025\001\255\255\255\255\255\255\029\001\
\030\001\031\001\032\001\033\001\034\001\035\001\036\001\037\001\
\020\001\255\255\255\255\255\255\024\001\025\001\255\255\255\255\
\255\255\255\255\255\255\031\001\032\001\033\001\034\001\035\001\
\036\001\037\001\020\001\255\255\017\001\255\255\024\001\025\001\
\021\001\022\001\023\001\255\255\255\255\026\001\027\001\033\001\
\034\001\035\001\036\001\037\001\017\001\255\255\255\255\255\255\
\021\001\022\001\023\001\255\255\255\255\026\001\027\001\028\001\
\029\001\030\001\031\001\032\001\017\001\255\255\255\255\255\255\
\021\001\022\001\023\001\255\255\255\255\026\001\027\001\028\001\
\029\001\030\001\031\001\032\001\017\001\255\255\255\255\255\255\
\021\001\022\001\023\001\255\255\255\255\026\001\027\001\028\001\
\029\001\030\001\017\001\255\255\255\255\255\255\021\001\022\001\
\023\001\255\255\255\255\026\001\027\001\028\001"

let yynames_const = "\
  FOR\000\
  IF\000\
  ELSE\000\
  WHILE\000\
  RETURN\000\
  SIZEOF\000\
  VOID\000\
  INT\000\
  CHAR\000\
  STRUCT\000\
  DOUBLE\000\
  LPAR\000\
  RPAR\000\
  LBRACE\000\
  RBRACE\000\
  LSQUARE\000\
  RSQUARE\000\
  SEMICOLON\000\
  COMMA\000\
  DOT\000\
  ARROW\000\
  EOF\000\
  EQ\000\
  OR\000\
  AND\000\
  PLUS\000\
  MINUS\000\
  STAR\000\
  SLASH\000\
  PERCENT\000\
  PLUSPLUS\000\
  MINUSMINUS\000\
  BANG\000\
  AMPERSAND\000\
  "

let yynames_block = "\
  IDENT\000\
  CINT\000\
  CDOUBLE\000\
  CSTR\000\
  EQOP\000\
  COMP\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'decl_star) in
    Obj.repr(
# 86 "parser.mly"
    ( List.rev _1 )
# 462 "parser.ml"
               : Ast.loc Ast.file))
; (fun __caml_parser_env ->
    Obj.repr(
# 92 "parser.mly"
                ( [] )
# 468 "parser.ml"
               : 'decl_star))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'decl_star) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'decl) in
    Obj.repr(
# 93 "parser.mly"
                 ( _2 :: _1 )
# 476 "parser.ml"
               : 'decl_star))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'decl_vars) in
    Obj.repr(
# 98 "parser.mly"
    ( Dvars _1 )
# 483 "parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : 'ident) in
    let _4 = (Parsing.peek_val __caml_parser_env 2 : 'decl_vars_star) in
    Obj.repr(
# 100 "parser.mly"
    ( Dstruct (_2, _4) )
# 491 "parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 4 : 'c_type_fun) in
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'parameters_opt) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 102 "parser.mly"
    ( let ty, id = _1 in Dfun (ty, id, _3, _5) )
# 500 "parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'c_type) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'var) in
    Obj.repr(
# 106 "parser.mly"
             ( declarator _1 _2 )
# 508 "parser.ml"
               : 'c_type_fun))
; (fun __caml_parser_env ->
    Obj.repr(
# 111 "parser.mly"
                ( [] )
# 514 "parser.ml"
               : 'decl_vars_star))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'decl_vars) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'decl_vars_star) in
    Obj.repr(
# 112 "parser.mly"
                           ( _1 @ _2 )
# 522 "parser.ml"
               : 'decl_vars_star))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'c_type) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'vars) in
    Obj.repr(
# 116 "parser.mly"
                        ( List.map (fun d -> (declarator _1 d)) _2 )
# 530 "parser.ml"
               : 'decl_vars))
; (fun __caml_parser_env ->
    Obj.repr(
# 120 "parser.mly"
                ( [] )
# 536 "parser.ml"
               : 'parameters_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'parameters) in
    Obj.repr(
# 121 "parser.mly"
                ( _1 )
# 543 "parser.ml"
               : 'parameters_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'c_type) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'var) in
    Obj.repr(
# 125 "parser.mly"
             ( [declarator _1 _2] )
# 551 "parser.ml"
               : 'parameters))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'c_type) in
    let _2 = (Parsing.peek_val __caml_parser_env 2 : 'var) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'parameters) in
    Obj.repr(
# 126 "parser.mly"
                              ( declarator _1 _2 :: _4 )
# 560 "parser.ml"
               : 'parameters))
; (fun __caml_parser_env ->
    Obj.repr(
# 130 "parser.mly"
       ( Tvoid )
# 566 "parser.ml"
               : 'c_type))
; (fun __caml_parser_env ->
    Obj.repr(
# 131 "parser.mly"
      ( Tint )
# 572 "parser.ml"
               : 'c_type))
; (fun __caml_parser_env ->
    Obj.repr(
# 132 "parser.mly"
       ( Tchar )
# 578 "parser.ml"
               : 'c_type))
; (fun __caml_parser_env ->
    Obj.repr(
# 133 "parser.mly"
         ( Tdouble )
# 584 "parser.ml"
               : 'c_type))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'ident) in
    Obj.repr(
# 134 "parser.mly"
               ( Tstruct _2 )
# 591 "parser.ml"
               : 'c_type))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'c_type) in
    Obj.repr(
# 137 "parser.mly"
          ( _1 )
# 598 "parser.ml"
               : 'cplx_type))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'cplx_type) in
    Obj.repr(
# 138 "parser.mly"
                  ( Tpointer(_1) )
# 605 "parser.ml"
               : 'cplx_type))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'var) in
    Obj.repr(
# 142 "parser.mly"
      ( [_1] )
# 612 "parser.ml"
               : 'vars))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'var) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'vars) in
    Obj.repr(
# 143 "parser.mly"
                 ( _1 :: _3 )
# 620 "parser.ml"
               : 'vars))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'ident) in
    Obj.repr(
# 148 "parser.mly"
    ( Dident _1 )
# 627 "parser.ml"
               : 'var))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'var) in
    Obj.repr(
# 150 "parser.mly"
    ( Dpointer _2 )
# 634 "parser.ml"
               : 'var))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 156 "parser.mly"
    ( loc (Eassign (_1, _3)) )
# 642 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 158 "parser.mly"
    ( loc (Eunop (Upre_dec, _2)) )
# 649 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 160 "parser.mly"
    ( loc (Eunop (Upre_inc, _2)) )
# 656 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 162 "parser.mly"
    ( loc (Eunop (Upost_inc, _1)) )
# 663 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 164 "parser.mly"
    ( loc (Eunop (Upost_dec, _1)) )
# 670 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 166 "parser.mly"
    ( loc (Ebinop (Bor, _1, _3)) )
# 678 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 168 "parser.mly"
    ( loc (Ebinop (Band, _1, _3)) )
# 686 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Ast.binop) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 170 "parser.mly"
    ( loc (Ebinop (_2, _1, _3)) )
# 695 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Ast.binop) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 172 "parser.mly"
    ( loc (Ebinop (_2, _1, _3)) )
# 704 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 174 "parser.mly"
    ( loc (Ebinop (Badd, _1, _3)) )
# 712 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 176 "parser.mly"
    ( loc (Ebinop (Bsub, _1, _3)) )
# 720 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 178 "parser.mly"
    ( loc (Ebinop (Bmul, _1, _3)) )
# 728 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 180 "parser.mly"
    ( loc (Ebinop (Bdiv, _1, _3)) )
# 736 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 182 "parser.mly"
    ( loc (Ebinop (Bmod, _1, _3)) )
# 744 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'cplx_type) in
    Obj.repr(
# 184 "parser.mly"
    ( loc (Esizeof (_3)) )
# 751 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 186 "parser.mly"
    ( loc (Eunop (Uplus, _2)) )
# 758 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 188 "parser.mly"
    ( loc (Eunop (Uminus, _2)) )
# 765 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 190 "parser.mly"
    ( loc (Eunop (Unot, _2)) )
# 772 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 192 "parser.mly"
    ( loc (Eunop (Uamp, _2)) )
# 779 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : float) in
    Obj.repr(
# 194 "parser.mly"
    ( loc (Econst (Cdouble _1)) )
# 786 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int32) in
    Obj.repr(
# 196 "parser.mly"
    ( if _1 = 0l then loc Enull else loc (Econst (Cint _1)) )
# 793 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 198 "parser.mly"
    ( loc (Econst (Cstring _1)) )
# 800 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'ident) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'l_expr_opt) in
    Obj.repr(
# 200 "parser.mly"
    ( loc (Ecall (_1, _3)) )
# 808 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'ident) in
    Obj.repr(
# 202 "parser.mly"
    ( loc (Eident _1) )
# 815 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 204 "parser.mly"
    ( loc (Eunop (Ustar, _2)) )
# 822 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 206 "parser.mly"
    (
      let sum = loc (Ebinop(Badd, _1, _3)) in
	loc (Eunop(Ustar,sum))
    )
# 833 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'ident) in
    Obj.repr(
# 211 "parser.mly"
    ( loc (Edot (_1, _3)) )
# 841 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'ident) in
    Obj.repr(
# 214 "parser.mly"
    (
      let star = loc (Eunop(Ustar, _1)) in
      loc (Edot (star, _3))
    )
# 852 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 219 "parser.mly"
    ( _2 )
# 859 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 224 "parser.mly"
   ( loc Sskip )
# 865 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 226 "parser.mly"
   ( loc (Sexpr _1) )
# 872 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'statement) in
    Obj.repr(
# 228 "parser.mly"
   ( loc (Sif (_3, _5, loc_dummy Sskip)) )
# 880 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 4 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 2 : 'statement) in
    let _7 = (Parsing.peek_val __caml_parser_env 0 : 'statement) in
    Obj.repr(
# 230 "parser.mly"
   ( loc (Sif (_3, _5, _7)) )
# 889 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 6 : 'l_expr_opt) in
    let _5 = (Parsing.peek_val __caml_parser_env 4 : 'expr_or_1) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'l_expr_opt) in
    let _9 = (Parsing.peek_val __caml_parser_env 0 : 'statement) in
    Obj.repr(
# 232 "parser.mly"
   ( let l_expr i = List.map (fun e -> loc_i i (Sexpr e)) in
     loc (Sfor (l_expr 3 _3, _5, l_expr 7 _7, _9)) )
# 900 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'statement) in
    Obj.repr(
# 235 "parser.mly"
   ( loc (Swhile (_3, _5)) )
# 908 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 237 "parser.mly"
   ( loc (Sblock _1) )
# 915 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    Obj.repr(
# 239 "parser.mly"
   ( loc (Sreturn None) )
# 921 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 241 "parser.mly"
   ( loc (Sreturn (Some _2)) )
# 928 "parser.ml"
               : 'statement))
; (fun __caml_parser_env ->
    Obj.repr(
# 245 "parser.mly"
                (  loc (Econst (Cint 1l)) )
# 934 "parser.ml"
               : 'expr_or_1))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 246 "parser.mly"
       ( _1 )
# 941 "parser.ml"
               : 'expr_or_1))
; (fun __caml_parser_env ->
    Obj.repr(
# 250 "parser.mly"
                ( [] )
# 947 "parser.ml"
               : 'l_expr_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'l_expr) in
    Obj.repr(
# 251 "parser.mly"
                ( _1 )
# 954 "parser.ml"
               : 'l_expr_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 255 "parser.mly"
       ( [_1] )
# 961 "parser.ml"
               : 'l_expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'l_expr) in
    Obj.repr(
# 256 "parser.mly"
                    ( _1 :: _3 )
# 969 "parser.ml"
               : 'l_expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 260 "parser.mly"
                ( [] )
# 975 "parser.ml"
               : 'statement_star))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'statement) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'statement_star) in
    Obj.repr(
# 261 "parser.mly"
                           ( _1 :: _2 )
# 983 "parser.ml"
               : 'statement_star))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 : 'decl_vars_star) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'statement_star) in
    Obj.repr(
# 265 "parser.mly"
                                              ( _2, _3 )
# 991 "parser.ml"
               : 'block))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 271 "parser.mly"
        ( loc _1 )
# 998 "parser.ml"
               : 'ident))
(* Entry file *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let file (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Ast.loc Ast.file)
