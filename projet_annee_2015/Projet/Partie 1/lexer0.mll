(* Analyseur lexical pour un sous-ensemble du langage C *)

{
  open Lexing
  open Parser

  (* Exception pour les erreurs lexicales, le string donne précisément la cause de l'erreur.  *)
  exception ErreurLexicale of string

  (* Liste associant aux mots-clés du langage leur token *)
  let kwd_tbl = ["char", CCHAR; "double", CDOUBLE; "int", CINT; "struct", STRUCT; "void", VOID;
                 "if", IF; "else", ELSE; "for", FOR; "while", WHILE; 
                 "sizeof", SIZEOF; "return", RETURN]

  (* Fonction qui détermine si la chaîne lue est un mot-clé ou un identificateur *)
  let id_or_kwd s = try List.assoc s kwd_tbl with _ -> IDENT s
}

(* Expressions régulières utiles pour la suite *)
let letter = ['a'-'z' 'A'-'Z' '_']
let digit = ['0'-'9']
let ident = letter (letter | digit)*
let integer = ['0'-'9']+
let float = (digit+'.'digit* | '.'digit+)(('e'|'E')'-'? digit+)?
let blank = [' ' '\t' '\r']
let comment = "//"[^'\n']*('\n' | eof)
let include = "#include <" letter+ ".h>"

(* 
  Toutes les suites de caractères possibles dans notre langage.
  On ignore les blancs, les commentaires, ainsi que les include.
  Tout caractère ne respectant pas l'une des règles ci-dessous lève l'exception Erreurlexicale
  Sinon on renvoie le token correspondant a ce qui a été lu.
*)
rule token = parse
  | blank+                  { token lexbuf }
  | comment                 { Lexing.new_line lexbuf; token lexbuf }
  | "/*"                    { comment lexbuf}
  | '\n'                    { Lexing.new_line lexbuf; token lexbuf }
  | include                 { token lexbuf }
  | ident as id             { id_or_kwd id }
  | '{'                     { LA }
  | '}'                     { RA }
  | '('                     { LP }
  | ')'                     { RP }
  | '['                     { LC }
  | ']'                     { RC }
  | '*'                     { POINTER }
  | ','                     { SEP }
  | '.'                     { DOT }
  | "->"                    { ARROW }
  | '='                     { MOVE }
  | '+'                     { PLUS }
  | '-'                     { MINUS }
  | "++"                    { INCR }
  | "--"                    { DECR }
  | '&'                     { ADDR }
  | '!'                     { NOT }
  | "=="                    { EQUAL }
  | "!="                    { DIFF }
  | '<'                     { INF }
  | "<="                    { INFEG }
  | '>'                     { SUP }
  | ">="                    { SUPEG }
  | '/'                     { DIV }
  | '%'                     { MOD }
  | "&&"                    { AND }
  | "||"                    { OR }
  | ';'                     { END }
  | integer as i            { INT (int_of_string i) }
  | '\''                    { chaine_or_car "" true lexbuf }
  | '\"'                    { chaine_or_car "" false lexbuf }
  | float as f              { DOUBLE (float_of_string f) }
  | eof                     { EOF }
  | _ as c                  { raise (ErreurLexicale (String.make 1 c) ) }

  (* 
    Règle pour les commentaires sur plusieurs lignes. 
    Si on atteint la fin du fichier, on lève l'exception Erreurlexicale pour commentaire non terminé 
  *)
  and comment = parse
  | "*/"      { token lexbuf }
  | '\n'      { Lexing.new_line lexbuf; comment lexbuf }
  | _         { comment lexbuf }
  | eof       { raise (ErreurLexicale ("commentaire non termine")) }

  (* Règle pour lire les caractères normaux et spéciaux *)
  and getchar = parse 
  | [^'\'' '\"' '\\'] as c                                                   { c }
  | "\\\\"                                                                   { '\\' }
  | "\\x" ((digit | ['a'-'f' 'A'-'F']) (digit | ['a'-'f' 'A'-'F']) as hex)   { Char.chr (int_of_string ("0x" ^ hex)) }
  | "\\\'"                                                                   { '\'' }
  | "\\\""                                                                   { '\"' }

  (* 
    Règle pour construire une chaîne de caractère ou un caractère.
    Si on lit un caractère, le flag caractere vaut true et on s'attend à lire un seul caractère finissant par un quote.
    Si on lit une chaine, le flag caractere vaut false et on s'attend à ce que la chaine finisse par un guillement.
    Atteindre la fin du fichier signifie que le caractère ou la chaîne est mal construite.
    La première règle permet de déterminer le caractère grâce à getchar. On lit la chaîne "" afin de ne pas consommer le caractère.
  *)
  and chaine_or_car construction caractere = parse
  | ""        { chaine_or_car (construction ^ (String.make 1 (getchar lexbuf))) caractere lexbuf }
  | '\''    { if caractere then 
              begin 
                if (String.length construction) = 1 then 
                 CHAR (construction.[0]) 
                else 
                  raise (ErreurLexicale ("caractere non valide")) 
                end 
             else 
              raise (ErreurLexicale "chaine non valide") 
            }
  | '\"'    { if not caractere then STRING (construction) else raise (ErreurLexicale "caractere non valide") }
  | eof     { if not caractere then raise (ErreurLexicale "chaine non finie") else raise (ErreurLexicale "caractere non fini") }