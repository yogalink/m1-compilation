/* Analyseur syntaxique pour un sous-ensemble du langage C */

%{
	open Ast
%}

/* Définitions des tokens

	Dans l'ordre:

	void  int  char  float  struct
	{  }  (  )  [  ] 
	*  ,  ;
	if  else
	.  ->  =
	++  --  &  !
	+  -
	==  !=  <  <=  >  >=  
	for  while
	sizeof  return
	un entier
	un caractère
	une chaîne de caractère
	un flottant
	un identificateur
	la fin du fichier
*/
%token VOID CINT CCHAR CDOUBLE STRUCT
%token LA RA LP RP LC RC
%token POINTER SEP END
%token IF ELSE
%token DOT ARROW MOVE
%token INCR DECR ADDR NOT
%token PLUS MINUS 
%token EQUAL DIFF INF INFEG SUP SUPEG DIV MOD AND OR
%token FOR WHILE
%token SIZEOF RETURN
%token <int> INT
%token <char> CHAR
%token <string> STRING
%token <float> DOUBLE
%token <string> IDENT
%token EOF



/* Définitions des priorités et associativités des tokens */
%right MOVE
%left OR
%left AND
%left EQUAL DIFF
%left INF INFEG SUP SUPEG
%left PLUS MINUS
%left MUL DIV MOD
%right NOT INCR DECR ADDR POINTER POS NEG
%left ARROW DOT LC
%nonassoc THEN 
%nonassoc ELSE

/* Point d'entrée de la grammaire */
%start fichier

/* Type des valeurs retournées par l'analyseur syntaxique */
%type <Ast.fichier> fichier

/* 
	Analyse syntaxique
	Le fichier source doit respecter la syntaxe suivante, après analyse lexicale.
	Ceci correspond à la grammaire donnée dans le sujet
*/

%%
fichier:
| EOF			{ [] }
| decl fichier 	{ $1 :: $2 }
;

decl:
| decl_vars 									{ Dvars $1 }
| STRUCT ident LA RA END 						{ Dtype ($2, []) }
| STRUCT ident LA ldecl_vars RA END 			{ Dtype ($2, $4) }
| ctype var LP RP bloc						{ Dfct ($1, $2, [], $5) }
| ctype var LP argument RP bloc				{ Dfct ($1, $2, $4, $6) } 
;

decl_vars:
| ctype vars END 	{ ($1, $2) }			
;

ldecl_vars:
| decl_vars	 			{ [$1] }
| decl_vars ldecl_vars	{ $1 :: $2 }
;

argument:
| ctype var 				{ [($1, $2)] }
| ctype var SEP argument 	{ ($1, $2) :: $4 }
;

expr:
| INT 								{ Int $1 }
| CHAR 								{ Char $1 }
| STRING 							{ String $1 }
| DOUBLE 							{ Double $1 }
| IDENT 							{ Ident ($1, (Parsing.symbol_start_pos (), Parsing.symbol_end_pos ())) }
| LP expr RP 						{ $2 }
| expr LC expr RC 					{ Array ($1, $3) }
| expr DOT ident 					{ Field ($1, $3) }
| expr ARROW ident 					{ Pfield ($1, $3) }
| expr MOVE expr 					{ Move ($1, $3) }
| ident LP RP 						{ Call ($1, []) }
| ident LP lexpr RP 				{ Call ($1, $3) }
| INCR expr 						{ Lincr $2 }
| DECR expr 						{ Ldecr $2 }
| expr INCR 	 					{ Rincr $1 }
| expr DECR 						{ Rdecr $1 }
| ADDR expr 						{ Addr $2 }
| NOT expr 							{ Not $2 }
| MINUS expr %prec NEG				{ Neg $2 }
| PLUS expr %prec POS				{ Pos $2 }
| expr EQUAL expr					{ Equal ($1, $3) }
| expr DIFF expr					{ Diff ($1, $3) }
| expr INF expr						{ Inf ($1, $3) }
| expr INFEG expr					{ Infeg ($1, $3) }
| expr SUP expr						{ Sup ($1, $3) }
| expr SUPEG expr					{ Supeg ($1, $3) }
| expr PLUS expr					{ Add ($1, $3) }
| expr MINUS expr					{ Sub ($1, $3) }
| expr POINTER expr %prec MUL 		{ Mul ($1, $3) }
| expr DIV expr						{ Div ($1, $3) }
| expr MOD expr						{ Mod ($1, $3) }
| expr AND expr						{ And ($1, $3) }
| expr OR expr						{ Or ($1, $3) }
| SIZEOF LP ctype RP 				{ Sizeof $3 }
;

lexpr:
| expr 					{ [$1] }
| expr SEP lexpr 		{ $1 :: $3 }
;

oexpr:
| 			{ None }
| expr 		{ Some $1 }

instr:
| END 										{ End }
| expr END 									{ Expr $1 }
| bloc 										{ Bloc $1 }
| IF LP expr RP instr %prec THEN			{ If ($3, $5) }
| IF LP expr RP instr ELSE instr			{ Ifelse ($3, $5, $7) }
| WHILE LP expr RP instr 					{ While ($3, $5) }
| FOR LP END oexpr END RP instr 			{ For ([], $4, [], $7) }
| FOR LP lexpr END oexpr END RP instr		{ For ($3, $5, [], $8) }
| FOR LP END oexpr END lexpr RP instr 		{ For ([], $4, $6, $8) }
| FOR LP lexpr END oexpr END lexpr RP instr { For ($3, $5, $7, $9) }
| RETURN oexpr END 							{ Return $2 }
;

linstr:
| instr 					{ [$1] }
| instr linstr 				{ $1 :: $2 }
;

bloc:
| LA RA 					{ ([], []) }
| LA ldecl_vars RA 			{ ($2, []) }
| LA linstr RA 				{ ([], $2) }
| LA ldecl_vars linstr RA 	{ ($2, $3) }
;

ctype:
| POINTER ctype 		{ Cpointer $2 }
| STRUCT ident 			{ Cstruct $2 }
| VOID	 				{ Cvoid }
| CINT 					{ Cint }
| CCHAR 				{ Cchar }
| CDOUBLE 				{ Cdouble }
;

vars:
| var 					{ [$1] }
| var SEP vars 			{ $1 :: $3 }
;

var:
| POINTER var 			{ Vpointer $2 }
| ident 				{ Vident $1 }
;

ident:
| IDENT 				{ ($1, (Parsing.symbol_start_pos (), Parsing.symbol_end_pos ())) }
;