(* Fichier principal du compilateur du sous-ensemble du langage C *)

open Ast
open Format
open Lexing
open Printf

(* localise une erreur en indiquant la ligne et la colonne *)
let localisation file pos =
  let l = pos.pos_lnum in
  let c = pos.pos_cnum - pos.pos_bol + 1 in
  eprintf "File \"%s\", line %d, characters %d-%d:\n" file l (c-1) c

(* Fonctions d'affichage de notre syntaxe abstraite *)
let print_ident s =
  sprintf "%s" (fst s)

let rec print_type t =
  match t with
  | Cvoid -> "void"
  | Cint -> "int"
  | Cchar -> "char"
  | Cdouble -> "double"
  | Cpointer (pt) -> "*" ^ (print_type pt)
  | Cstruct (ident) -> "struct " ^ (print_ident ident)

let rec print_var var =
  match var with
  | Vident (i) -> (print_ident i) ^ ","
  | Vpointer (v) -> "*" ^ (print_var v)

let print_vars vars =
  let s = List.fold_left (fun acc a -> acc  ^ print_var a) "" vars 
  in (String.sub s 0 (String.length s - 1) ) ^ ";" 

let print_dvars v =
  (print_type (fst v)) ^ " " ^ (print_vars (snd v)) ^ "\n"

let print_dtype i v =
  let vars = List.fold_left (fun acc a -> acc ^ print_dvars a ) "" v
  in sprintf "struct %s{\n%s};\n" (print_ident i) vars

let rec print_expr e =
  match e with
  | Int i -> sprintf "%d" i
  | Char c -> sprintf "\'%c\'" c
  | String s -> sprintf "%S" s
  | Double d -> sprintf "%f" d
  | Ident id -> print_ident id
  | Array (e1, e2) -> sprintf "%s[%s]" (print_expr e1) (print_expr e2)
  | Field (e, i) -> sprintf "%s.%s" (print_expr e) (print_ident i)
  | Pfield (e, i) -> sprintf "%s->%s" (print_expr e) (print_ident i)
  | Move (e1, e2) -> sprintf "%s=%s" (print_expr e1) (print_expr e2)
  | Call (i, le) ->  sprintf "%s(%s)" (print_ident i) (print_lexpr le)
  | Lincr e -> sprintf "++%s" (print_expr e)
  | Ldecr e -> sprintf "--%s" (print_expr e)
  | Rincr e -> sprintf "%s++" (print_expr e)
  | Rdecr e -> sprintf "%s--" (print_expr e)
  | Addr e -> sprintf "&%s" (print_expr e)
  | Not e -> sprintf "!%s" (print_expr e)
  | Neg e -> sprintf "-%s" (print_expr e) 
  | Pos e -> sprintf "+%s" (print_expr e)
  | Equal (e1, e2) -> sprintf "%s==%s" (print_expr e1) (print_expr e2)
  | Diff (e1, e2) -> sprintf "%s!=%s" (print_expr e1) (print_expr e2)
  | Inf (e1, e2) -> sprintf "%s<%s" (print_expr e1) (print_expr e2)
  | Infeg (e1, e2) -> sprintf "%s<=%s" (print_expr e1) (print_expr e2)
  | Sup (e1, e2) -> sprintf "%s>%s" (print_expr e1) (print_expr e2)
  | Supeg (e1, e2) -> sprintf "%s>=%s" (print_expr e1) (print_expr e2)
  | Add (e1, e2) -> sprintf "%s+%s" (print_expr e1) (print_expr e2)
  | Sub (e1, e2) -> sprintf "%s-%s" (print_expr e1) (print_expr e2)
  | Mul (e1, e2) -> sprintf "%s*%s" (print_expr e1) (print_expr e2)
  | Div (e1, e2) -> sprintf "%s/%s" (print_expr e1) (print_expr e2)
  | Mod (e1, e2) -> sprintf "%smod%s" (print_expr e1) (print_expr e2)
  | And (e1, e2) -> sprintf "%s&&%s" (print_expr e1) (print_expr e2)
  | Or (e1, e2) -> sprintf "%s||%s" (print_expr e1) (print_expr e2)
  | Sizeof t -> sprintf "sizeof(%s)" (print_type t)

and print_lexpr le =
  let rec pre l acc =
    match l with
    | [] -> acc
    | x::s -> pre s (acc ^ print_expr x ^ ",")
  in let s = (pre le "") in sprintf "(%s)" (String.sub s 0 (String.length s - 
  if String.length s = 0 then 0 else 1))

let print_oexpr e = 
  match e with
  | Some exp -> print_expr exp
  | None -> ""

let rec print_instr i =
  match i with
  | If (e, i) -> sprintf "If(%s)\n%s" (print_expr e) (print_instr i)
  | Ifelse (e, i1, i2) -> sprintf "If(%s)\n%s\nelse\n%s" (print_expr e) (print_instr i1) (print_instr i2)
  | While (e, i) -> sprintf "while(%s)\n%s" (print_expr e) (print_instr i)
  | For (le1, e, le2, i) -> sprintf "for(%s;%s;%s)\n%s" 
                            (print_lexpr le1) (print_oexpr e)(print_lexpr le2) (print_instr i)
  | Return e -> sprintf "return %s;\n" (print_oexpr e)
  | Bloc b -> print_bloc b
  | Expr e -> sprintf "%s;\n" (print_expr e)
  | End -> ";\n"

and print_bloc b = 
  let vars = List.fold_left (fun acc a -> acc ^ print_dvars a ) "" (fst b) in
  let instr = List.fold_left (fun acc a -> acc ^ print_instr a ) "" (snd b) in
  sprintf "{\n%s\n%s}" vars instr

let print_dfct t v l b =
  let s = List.fold_left (fun acc a -> acc ^ (print_type (fst a)) ^ " " ^(print_var (snd a))) "" l in
  sprintf "%s %s(%s)%s" (print_type t) (print_var v) (String.sub s 0 (String.length s - 
  if String.length s = 0 then 0 else 1)) (print_bloc b)

let rec print_fichier f = 
  match f with
  | [] -> ()
  | Dvars v :: s -> printf "%s" (print_dvars v); print_fichier s
  | Dtype (i, v) :: s -> printf "%s" (print_dtype i v); print_fichier s
  | Dfct (t, i, l, b) :: s -> printf "%s" (print_dfct t i l b); print_fichier s

let () = 
  (* On lit le nom du fichier sur la ligne de commande *)
  let file =  
    if (Array.length Sys.argv < 2) then
    begin
      eprintf "Erreur: un fichier doit être donné en paramètre\n"; 
      exit 1
    end
    else
      Sys.argv.(1)
  in

  (* Ce fichier doit avoir l'extension .c *)
  if not (Filename.check_suffix file ".c") then 
  begin
    eprintf "Le fichier d'entrée doit avoir l'extension .c\n@?";
    exit 1
  end;
    
  (* Ouverture du fichier source en lecture *)
  let f = open_in file in
      
  (* Création d'un tampon d'analyse lexicale *)
  let buf = Lexing.from_channel f in
  try
    (* Parsing: la fonction  Parser.fichier transforme le tampon lexical en un 
      arbre de syntaxe abstraite si aucune erreur (lexicale ou syntaxique) 
      n'est détectée.
      La fonction Lexer.token est utilisée par Parser.fichier pour obtenir 
      le prochain token. *)
    let p = Parser.fichier Lexer.token buf in
    close_in f;
    (*
    print_fichier p;
    *)
    exit 0
  with
    | Lexer.ErreurLexicale c -> 
    (* Erreur lexicale. On récupère sa position absolue et 
       on la convertit en numéro de ligne *)
      localisation file (Lexing.lexeme_start_p buf);
      eprintf "Erreur dans l'analyse lexicale: %s.\n" c;
      exit 1
    | Parsing.Parse_error -> 
    (* Erreur syntaxique. On récupère sa position absolue et on la 
       convertit en numéro de ligne *)
    localisation file (Lexing.lexeme_start_p buf);
    eprintf "Erreur dans l'analyse syntaxique\n";
    exit 1
    | _ ->
    eprintf "Erreur inconnue\n";
    exit 2