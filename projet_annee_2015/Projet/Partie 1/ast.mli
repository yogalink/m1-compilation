(* Déclarations des types utilisés pour créer le mini-compilateur *)

type pos = Lexing.position * Lexing.position

(* Identificateur *)
type ident = string * pos

(* Variable (identificateur avec une ou plusieurs étoiles) *)
type var = 
	| Vident of ident
	| Vpointer of var

(* Ensemble de variables *)
type vars = var list

(* 
	Types C existants : void, int, char, float, pointeur de type, structure
*)
type ctype = 
	| Cvoid
	| Cint
	| Cchar
	| Cdouble
	| Cpointer of ctype
	| Cstruct of ident

(* Déclarations multiples de variables d'un type *)
type decl_vars = ctype * vars

(* Argument pour les fonctions *)
type argument = ctype * var

(* 
	Toutes les expressions possibles, dans l'ordre:
	entier, caractère, chaîne de caractère, flottant, identificateur
	tableau, champ, pointeur de champ
	affectation, appel de fonction
	incrémentation/décrémentation avant ou après utilisation
	adresse, complémentaire, négation, positif
	egalité, inégalité
	supérieur/inférieur avec ou sans égalité
	somme, différence, produit, quotient, reste
	et logique, ou logique
	taille en octet
*)
type expr = 
	| Int of int | Char of char | String of string | Double of float | Ident of ident
	| Array of expr * expr | Field of expr * ident | Pfield of expr * ident
	| Move of expr * expr | Call of ident * expr list
	| Lincr of expr | Rincr of expr | Ldecr of expr | Rdecr of expr
	| Addr of expr | Not of expr | Neg of expr | Pos of expr
	| Equal of expr * expr | Diff of expr * expr
	| Inf of expr * expr | Infeg of expr * expr | Sup of expr * expr | Supeg of expr * expr
	| Add of expr * expr | Sub of expr * expr | Mul of expr * expr | Div of expr * expr | Mod of expr * expr
	| And of expr * expr | Or of expr * expr
	| Sizeof of ctype

(* 
	Les instructions possibles: 
	instruction vide
	bloc
	expression
	test simple
	test complet
	boucle while
	boucle for
	retour de fonction
*)
type instr =
	| End
	| Bloc of bloc
	| Expr of expr
	| If of expr * instr
	| Ifelse of expr * instr * instr
	| While of expr * instr
	| For of expr list * expr option * expr list * instr
	| Return of expr option

(* Ensemble de déclarations de variables et ensemble d'instructions *)
and bloc = decl_vars list * instr list

(* 
	Les déclarations possibles:
	déclaration de variables
	déclaration de type
	déclaration de fonctions
*)
type decl = 
	| Dvars of decl_vars
	| Dtype of ident * decl_vars list
	| Dfct of ctype * var * argument list * bloc

(* Ensemble de déclaration *)
type fichier = decl list