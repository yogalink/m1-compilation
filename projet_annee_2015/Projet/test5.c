int x;

int max(int a, int b) {
	if(a>=b)
		return a;
	else
		return b;
}

int factorielle(int n) {
	if(n<=0)
		return 1;
	else return n * factorielle (n-1);
}

struct coord {
	int x;
	int y;
};

struct coord initCoord(int x, int y) {
	struct coord c;
	c.x = x;
	c.y = y;
	return c;
}

struct coord initDefaultCoord() {
	return initCoord(0, 0);
}

struct nul {int x;};

void afficherCoord(struct coord c) {
	int x;
	int y;
	x = c.x;
	y = c.y;
	printf("%s", "x = ");
	printf("%d", x);
	printf("%s", ", ");
	printf("%s", "y = ");
	printf("%d", y);
	printf("%s", "\n");
}

void println() {
	printf("%s", "\n");
}

void printStr(char *s) {
	printf("%s", s);
}

void printInt(int n) {
	printf("%d", n);
}

void printDbl(double f) {
	printf("%f", f);
}

struct nul po() {
	struct nul p;
	p.x = 50;
	return p;
}

int main() {
	struct coord c1;
	struct coord c2;
	struct nul p;
	int n;
	n = max(1, 5);
	x = factorielle(n);
	x = max(10, 20);
	printInt(x);
	println();
	printInt(max(10, 40));
	println();
	printInt(n);
	println();
	printInt(x);
	println();
	p = po();
	printInt(p.x);
	println();
	c1 = initDefaultCoord();
	c2 = initCoord(5, 10);
	afficherCoord(c1);
	afficherCoord(c2);
	return 0;
}