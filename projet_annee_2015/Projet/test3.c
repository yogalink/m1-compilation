int main() {
	int a;
	int b;
	char *c;
	char *d;
	char *e;
	char *f;
	char *g;
	char *h;
	int n;

	a = 10;
	b = 3;

	if(a == b)
		c = "egal: vrai";
	else
		c = "egal: faux";
	if(a != b)
		d = "different: vrai";
	else
		d = "different: faux";
	if(a < b)
		e = "inf: vrai";
	else
		e = "inf: faux";
	if(a <= b)
		f = "infeq: vrai";
	else
		f = "infeq: faux";
	if(a > b)
		g = "sup: vrai";
	else
		g = "sup: faux";
	if(a >= b)
		h = "supeq: vrai";
	else
		h = "supeq: faux";

	printf("%s", c);
	printf("%s", "\n");
	printf("%s", d);
	printf("%s", "\n");
	printf("%s", e);
	printf("%s", "\n");
	printf("%s", f);
	printf("%s", "\n");
	printf("%s", g);
	printf("%s", "\n");
	printf("%s", h);
	printf("%s", "\n");

	n = a + b;
	printf("%d", n);
	printf("%s", "\n");

	n = a - b;
	printf("%d", n);
	printf("%s", "\n");

	n = a * b;
	printf("%d", n);
	printf("%s", "\n");

	n = a / b;
	printf("%d", n);
	printf("%s", "\n");

	n = a % b;
	printf("%d", n);
	printf("%s", "\n");

	n = (a == 0) && (b == 20);
	printf("%d", n);
	printf("%s", "\n");

	n = (a == 0) || (b == 20);
	printf("%d", n);
	printf("%s", "\n");
}