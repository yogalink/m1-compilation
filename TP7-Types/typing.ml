open Ast
open Error

(* Informations données par l'environnement de typage. *)
type type_annot =
  | TA_var of typ
  | TA_fun of typ * (typ list)

(* Type de l'environnement de typage. *)
module Env = Map.Make(String)
type  tenv = type_annot Env.t

(* Environnement vide. *)
let empty_env = Env.empty
(* Environnement de base, à compléter pour tenir compte d'éventuelles fonctions
   ou variables primitives, par exemple [print_int] et [print_newline] si
   vous en avez fait des fonctions ordinaires (extension "uniformisation" du
   TP précédent). *)
let base_env = empty_env

(* Ajout à un nœud de l'information de type. *)
let upd_type ne ty = ne.typ <- ty

(* Fonction à appeler pour rapporter des erreurs de types. *)	
let type_error l t1 t2 = error (Type_error(t1, t2)) l

let not_implemented() = failwith "Not implemented"

let rec check_types l ty1 ty2 = 
  match ty1, ty2 with
    | Tunit, Tunit | Tbool, Tbool | Tint, Tint -> ()
    | _, _ -> type_error l ty1 ty2
 
let rec type_expr t_env ne =
  match ne.expr with
       
  | Econst Cunit     ->
      (* Met à jour le type du nœud. *)
     upd_type ne Tunit      
  | Econst Cbool b -> upd_type ne Tbool
  | Econst Cint i ->  upd_type ne Tint
     
  | Eif (c,th,el) ->
    type_expr t_env c;
    type_expr t_env th;
    type_expr t_env el;
    check_types c.pos Tbool c.typ;
    check_types th.pos th.typ el.typ;
    upd_type ne th.typ
      
  | Ebinop ((Beq | Bneq), op1, op2) ->
     
     type_expr t_env op1;
    type_expr t_env op2;
    check_types op1.pos op1.typ op2.typ;
    upd_type ne Tbool
  | Ebinop((Blt | Ble | Bgt | Bge), op1, op2) ->
     type_expr t_env op1;
    type_expr t_env op2;
    check_types op1.pos op1.typ Tint;
    check_types op2.pos op2.typ Tint;
    upd_type ne Tbool
	 
  | Ebinop ((Band | Bor), op1, op2) -> 
     type_expr t_env op1;
    type_expr t_env op2;
    check_types op1.pos Tbool op1.typ;
    check_types op2.pos Tbool op2.typ;
    upd_type ne Tbool
      
  | Eprint_newline e ->
     (* Calcule le type de l'expression [e], et met à jour le
	nœud représentant [e]. *)
     type_expr t_env e;
    (* Vérifie que le type trouvé pour [e] est bien le type
       attendu [Tunit]. *)
    check_types e.pos Tunit e.typ;
    (* Met à jour le type de l'expression complète. *)
    upd_type ne Tunit
      
  | Eprint_int e ->
     type_expr t_env e;
    check_types e.pos Tint e.typ;
    upd_type ne Tunit
      
  | Eunop (Uminus , e) ->
     type_expr t_env e;
    check_types e.pos Tint e.typ;
    upd_type ne Tint
  | Eunop (Unot,  e) ->
         type_expr t_env e;
    check_types e.pos Tbool e.typ;
    upd_type ne Tbool
      
  | Ebinop ( (Badd | Bsub | Bmul | Bdiv) ,  e1, e2) ->
     type_expr t_env e1;
    type_expr t_env e2;         
    check_types e1.pos Tint e1.typ;
    check_types e2.pos Tint e2.typ;         
    upd_type ne Tint
      
  | Eapp (id , list_) ->
     let func_type =
       try Env.find id t_env
       with Not_found -> failwith ("Function type undefined.")
     in (match func_type with
     | TA_fun (typ, params) ->
	upd_type ne typ;
       List.iter2(fun (arg, orig) ->
	 (type_expr t_env arg;
	 check_types arg.pos arg.typ orig.typ;
	  (*match (arg, orig) with
	  | (a_x, a_p, a_t), (o_x,o_p,o_t) -> 
	    | (_,_,_),(_,_,_) -> failwith("Maybe unknown applied argument type.")*)
	 )) (list_ , params);
     | TA_var typ -> failwith ("Expected function type annotation, got variable instead.")
     | _ -> failwith ("No such type annotation."))
       
  | Eletin (id, e1, e2) -> (*| LET IDENT EQUAL expr IN expr*)
    type_expr t_env e1;
    let t_env = Env.add id (TA_var e1.typ) t_env in
    type_expr t_env e2;
    upd_type ne e2.typ
      
  | Eident id ->     	
     let f = 
       try Env.find id t_env
       with Not_found -> failwith ("Undefined variable type "^id)
     in match f with 
     | TA_var ty -> upd_type ne ty	  
     | _ -> failwith ("eident fail")
        
	
let type_prog p =
  (* On utilise une fonction auxiliaire, qui type une instruction et
     renvoie l'environnement de typage éventuellement mis à jour. *)
  let rec type_instr_list t_env = function
    | [] -> t_env

    | Icompute ne :: l ->
        type_expr t_env ne;
        type_instr_list t_env l

    | Ilet  (id, ty, ne ) :: l -> 
       let t_env = Env.add id (TA_var ty) t_env in 
       type_expr t_env ne;
       type_instr_list t_env l
	 
    | Ifun (b, id, params, func_typ, e) :: l ->
       let param_types = List.fold_left (fun lst_acc param ->
	 match param with
	   (param_id, param_typ) ->
	     let param_env = Env.add param_id (TA_var param_typ) t_env;
	     let param_lst = lst_acc @ [param_typ] in
	     param_lst
       ) [] params in
       let _ = Env.add id TA_fun(func_typ, param_types) t_env in ();
	 (* Ajouter la signature apres avoir vérifié les types peut-être *)
       type_expr t_env e;
       check_types e.pos func_typ e.typ;
       type_instr_list t_env l
	 
  in
  type_instr_list base_env p
(* Remarque : cette fonction renvoie l'environnement de typage final.
   Ça peut servir dans l'extension "optimisation : valeurs inutiles". *)
