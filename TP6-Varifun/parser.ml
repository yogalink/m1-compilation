type token =
  | AND
  | COMP of (Ast.binop)
  | CONST_BOOL of (bool)
  | CONST_INT of (int)
  | CONST_UNIT
  | DOUBLE_EQUAL
  | ELSE
  | EOF
  | EOI
  | EQUAL
  | IDENT of (string)
  | IF
  | IN
  | LET
  | LPAREN
  | MINUS
  | NEQ
  | NOT
  | OR
  | PLUS
  | PRINT_INT
  | PRINT_NEWLINE
  | RPAREN
  | SEMI
  | SLASH
  | STAR
  | THEN

open Parsing;;
let _ = parse_error;;
# 2 "parser.mly"

  open Ast

  let current_pos () =
    Parsing.symbol_start_pos (),
    Parsing.symbol_end_pos ()

  let mk_node e = { expr = e; pos = current_pos() }

# 43 "parser.ml"
let yytransl_const = [|
  257 (* AND *);
  261 (* CONST_UNIT *);
  262 (* DOUBLE_EQUAL *);
  263 (* ELSE *);
    0 (* EOF *);
  264 (* EOI *);
  265 (* EQUAL *);
  267 (* IF *);
  268 (* IN *);
  269 (* LET *);
  270 (* LPAREN *);
  271 (* MINUS *);
  272 (* NEQ *);
  273 (* NOT *);
  274 (* OR *);
  275 (* PLUS *);
  276 (* PRINT_INT *);
  277 (* PRINT_NEWLINE *);
  278 (* RPAREN *);
  279 (* SEMI *);
  280 (* SLASH *);
  281 (* STAR *);
  282 (* THEN *);
    0|]

let yytransl_block = [|
  258 (* COMP *);
  259 (* CONST_BOOL *);
  260 (* CONST_INT *);
  266 (* IDENT *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\003\000\003\000\005\000\005\000\005\000\
\004\000\004\000\004\000\004\000\004\000\004\000\004\000\004\000\
\004\000\004\000\004\000\004\000\004\000\004\000\004\000\004\000\
\006\000\006\000\006\000\000\000"

let yylen = "\002\000\
\002\000\000\000\002\000\002\000\005\000\001\000\003\000\001\000\
\001\000\002\000\003\000\003\000\003\000\003\000\006\000\002\000\
\003\000\003\000\003\000\003\000\003\000\006\000\002\000\002\000\
\001\000\001\000\001\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\026\000\027\000\025\000\008\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\028\000\000\000\000\000\
\000\000\009\000\006\000\000\000\000\000\000\000\000\000\000\000\
\016\000\023\000\024\000\001\000\003\000\000\000\000\000\000\000\
\004\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\007\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\014\000\013\000\000\000\000\000\000\000\000\000\
\000\000\005\000\000\000\000\000\000\000"

let yydgoto = "\002\000\
\014\000\015\000\016\000\017\000\018\000\019\000"

let yysindex = "\014\000\
\015\000\000\000\000\000\000\000\000\000\000\000\034\000\007\255\
\034\000\034\000\034\000\006\255\006\255\000\000\019\000\015\000\
\180\255\000\000\000\000\011\255\104\255\024\255\191\255\237\254\
\000\000\000\000\000\000\000\000\000\000\034\000\034\000\034\000\
\000\000\034\000\034\000\034\000\034\000\034\000\034\000\036\255\
\034\000\034\000\000\000\254\255\037\000\037\000\237\254\037\000\
\093\255\237\254\000\000\000\000\034\000\216\255\160\255\227\255\
\034\000\000\000\034\000\247\255\247\255"

let yyrindex = "\000\000\
\038\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\038\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\041\255\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\255\125\255\138\255\063\255\151\255\
\054\255\085\255\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\029\255\032\255"

let yygindex = "\000\000\
\000\000\052\000\000\000\249\255\001\000\000\000"

let yytablesize = 318
let yytable = "\021\000\
\020\000\023\000\024\000\025\000\038\000\039\000\020\000\020\000\
\003\000\004\000\005\000\020\000\026\000\027\000\001\000\006\000\
\022\000\020\000\028\000\009\000\040\000\020\000\044\000\045\000\
\046\000\020\000\047\000\048\000\049\000\050\000\051\000\052\000\
\042\000\054\000\055\000\015\000\015\000\002\000\022\000\022\000\
\015\000\010\000\010\000\022\000\053\000\056\000\010\000\010\000\
\010\000\060\000\015\000\061\000\010\000\022\000\015\000\010\000\
\010\000\022\000\010\000\010\000\021\000\021\000\010\000\012\000\
\012\000\021\000\010\000\029\000\012\000\012\000\012\000\021\000\
\000\000\000\000\012\000\021\000\000\000\012\000\012\000\021\000\
\012\000\012\000\000\000\000\000\012\000\011\000\011\000\000\000\
\012\000\000\000\011\000\011\000\011\000\030\000\031\000\000\000\
\011\000\000\000\032\000\011\000\011\000\000\000\011\000\011\000\
\030\000\031\000\011\000\034\000\035\000\032\000\011\000\037\000\
\000\000\000\000\000\000\000\000\038\000\039\000\034\000\035\000\
\000\000\036\000\037\000\000\000\000\000\019\000\019\000\038\000\
\039\000\041\000\019\000\019\000\019\000\000\000\000\000\000\000\
\019\000\000\000\017\000\017\000\019\000\000\000\019\000\017\000\
\017\000\017\000\019\000\000\000\000\000\017\000\019\000\018\000\
\018\000\017\000\000\000\017\000\018\000\018\000\018\000\017\000\
\030\000\031\000\018\000\017\000\000\000\032\000\018\000\058\000\
\018\000\000\000\000\000\059\000\018\000\000\000\034\000\035\000\
\018\000\036\000\037\000\000\000\030\000\031\000\000\000\038\000\
\039\000\032\000\000\000\033\000\000\000\000\000\000\000\030\000\
\031\000\000\000\034\000\035\000\032\000\036\000\037\000\000\000\
\000\000\000\000\000\000\038\000\039\000\034\000\035\000\000\000\
\036\000\037\000\000\000\000\000\043\000\000\000\038\000\039\000\
\030\000\031\000\000\000\000\000\000\000\032\000\057\000\000\000\
\000\000\000\000\000\000\030\000\031\000\000\000\034\000\035\000\
\032\000\036\000\037\000\000\000\000\000\000\000\059\000\038\000\
\039\000\034\000\035\000\000\000\036\000\037\000\000\000\030\000\
\031\000\000\000\038\000\039\000\032\000\000\000\000\000\031\000\
\000\000\000\000\000\000\032\000\000\000\034\000\035\000\000\000\
\036\000\037\000\000\000\000\000\034\000\035\000\038\000\039\000\
\037\000\003\000\004\000\005\000\000\000\038\000\039\000\000\000\
\006\000\007\000\000\000\008\000\009\000\010\000\000\000\011\000\
\000\000\000\000\012\000\013\000\003\000\004\000\005\000\000\000\
\000\000\000\000\000\000\006\000\007\000\000\000\020\000\009\000\
\010\000\000\000\011\000\034\000\000\000\012\000\013\000\037\000\
\000\000\000\000\000\000\000\000\038\000\039\000"

let yycheck = "\007\000\
\001\001\009\000\010\000\011\000\024\001\025\001\007\001\008\001\
\003\001\004\001\005\001\012\001\012\000\013\000\001\000\010\001\
\010\001\018\001\000\000\014\001\010\001\022\001\030\000\031\000\
\032\000\026\001\034\000\035\000\036\000\037\000\038\000\039\000\
\009\001\041\000\042\000\007\001\008\001\000\000\007\001\008\001\
\012\001\001\001\002\001\012\001\009\001\053\000\006\001\007\001\
\008\001\057\000\022\001\059\000\012\001\022\001\026\001\015\001\
\016\001\026\001\018\001\019\001\007\001\008\001\022\001\001\001\
\002\001\012\001\026\001\016\000\006\001\007\001\008\001\018\001\
\255\255\255\255\012\001\022\001\255\255\015\001\016\001\026\001\
\018\001\019\001\255\255\255\255\022\001\001\001\002\001\255\255\
\026\001\255\255\006\001\007\001\008\001\001\001\002\001\255\255\
\012\001\255\255\006\001\015\001\016\001\255\255\018\001\019\001\
\001\001\002\001\022\001\015\001\016\001\006\001\026\001\019\001\
\255\255\255\255\255\255\255\255\024\001\025\001\015\001\016\001\
\255\255\018\001\019\001\255\255\255\255\001\001\002\001\024\001\
\025\001\026\001\006\001\007\001\008\001\255\255\255\255\255\255\
\012\001\255\255\001\001\002\001\016\001\255\255\018\001\006\001\
\007\001\008\001\022\001\255\255\255\255\012\001\026\001\001\001\
\002\001\016\001\255\255\018\001\006\001\007\001\008\001\022\001\
\001\001\002\001\012\001\026\001\255\255\006\001\016\001\008\001\
\018\001\255\255\255\255\012\001\022\001\255\255\015\001\016\001\
\026\001\018\001\019\001\255\255\001\001\002\001\255\255\024\001\
\025\001\006\001\255\255\008\001\255\255\255\255\255\255\001\001\
\002\001\255\255\015\001\016\001\006\001\018\001\019\001\255\255\
\255\255\255\255\255\255\024\001\025\001\015\001\016\001\255\255\
\018\001\019\001\255\255\255\255\022\001\255\255\024\001\025\001\
\001\001\002\001\255\255\255\255\255\255\006\001\007\001\255\255\
\255\255\255\255\255\255\001\001\002\001\255\255\015\001\016\001\
\006\001\018\001\019\001\255\255\255\255\255\255\012\001\024\001\
\025\001\015\001\016\001\255\255\018\001\019\001\255\255\001\001\
\002\001\255\255\024\001\025\001\006\001\255\255\255\255\002\001\
\255\255\255\255\255\255\006\001\255\255\015\001\016\001\255\255\
\018\001\019\001\255\255\255\255\015\001\016\001\024\001\025\001\
\019\001\003\001\004\001\005\001\255\255\024\001\025\001\255\255\
\010\001\011\001\255\255\013\001\014\001\015\001\255\255\017\001\
\255\255\255\255\020\001\021\001\003\001\004\001\005\001\255\255\
\255\255\255\255\255\255\010\001\011\001\255\255\013\001\014\001\
\015\001\255\255\017\001\015\001\255\255\020\001\021\001\019\001\
\255\255\255\255\255\255\255\255\024\001\025\001"

let yynames_const = "\
  AND\000\
  CONST_UNIT\000\
  DOUBLE_EQUAL\000\
  ELSE\000\
  EOF\000\
  EOI\000\
  EQUAL\000\
  IF\000\
  IN\000\
  LET\000\
  LPAREN\000\
  MINUS\000\
  NEQ\000\
  NOT\000\
  OR\000\
  PLUS\000\
  PRINT_INT\000\
  PRINT_NEWLINE\000\
  RPAREN\000\
  SEMI\000\
  SLASH\000\
  STAR\000\
  THEN\000\
  "

let yynames_block = "\
  COMP\000\
  CONST_BOOL\000\
  CONST_INT\000\
  IDENT\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'instr_seq) in
    Obj.repr(
# 56 "parser.mly"
                ( _1 )
# 250 "parser.ml"
               : Ast.prog))
; (fun __caml_parser_env ->
    Obj.repr(
# 60 "parser.mly"
                 ( [] )
# 256 "parser.ml"
               : 'instr_seq))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'instr) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'instr_seq) in
    Obj.repr(
# 61 "parser.mly"
                        ( _1 :: _2 )
# 264 "parser.ml"
               : 'instr_seq))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 66 "parser.mly"
    ( Icompute _1 )
# 271 "parser.ml"
               : 'instr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 68 "parser.mly"
    ( Ilet (_2, _4) )
# 279 "parser.ml"
               : 'instr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'const) in
    Obj.repr(
# 73 "parser.mly"
    ( mk_node _1 )
# 286 "parser.ml"
               : 'simple_expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 75 "parser.mly"
    ( _2 )
# 293 "parser.ml"
               : 'simple_expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 77 "parser.mly"
    ( mk_node (Eident _1) )
# 300 "parser.ml"
               : 'simple_expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'simple_expr) in
    Obj.repr(
# 82 "parser.mly"
    ( _1 )
# 307 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 85 "parser.mly"
    ( mk_node (Eunop (Uminus, _2)) )
# 314 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 87 "parser.mly"
    ( mk_node (Ebinop (Badd, _1, _3)) )
# 322 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 89 "parser.mly"
    ( mk_node (Ebinop (Bsub, _1, _3)) )
# 330 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 91 "parser.mly"
    ( mk_node (Ebinop (Bmul, _1, _3)) )
# 338 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 93 "parser.mly"
    ( mk_node (Ebinop (Bdiv, _1, _3)) )
# 346 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _6 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 96 "parser.mly"
    ( mk_node (Eif (_2, _4, _6)) )
# 355 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 98 "parser.mly"
    ( mk_node (Eunop (Unot, _2)) )
# 362 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 100 "parser.mly"
    ( mk_node (Ebinop (Beq, _1, _3)) )
# 370 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 102 "parser.mly"
    ( mk_node (Ebinop (Bneq, _1, _3)) )
# 378 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Ast.binop) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 104 "parser.mly"
    ( mk_node (Ebinop (_2, _1, _3)) )
# 387 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 106 "parser.mly"
    ( mk_node (Ebinop (Band, _1, _3)) )
# 395 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 108 "parser.mly"
    ( mk_node (Ebinop (Bor, _1, _3)) )
# 403 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _6 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 111 "parser.mly"
    ( mk_node (Eletin (_2, _4, _6)) )
# 412 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'simple_expr) in
    Obj.repr(
# 114 "parser.mly"
    ( mk_node (Eprint_int _2) )
# 419 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'simple_expr) in
    Obj.repr(
# 116 "parser.mly"
    ( mk_node (Eprint_newline _2) )
# 426 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 123 "parser.mly"
    ( Econst Cunit )
# 432 "parser.ml"
               : 'const))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : bool) in
    Obj.repr(
# 125 "parser.mly"
    ( Econst (Cbool _1) )
# 439 "parser.ml"
               : 'const))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 127 "parser.mly"
    ( Econst (Cint _1) )
# 446 "parser.ml"
               : 'const))
(* Entry prog *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let prog (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Ast.prog)
