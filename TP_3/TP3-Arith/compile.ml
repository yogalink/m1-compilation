open Ast
open Mips
open Format



(* Constantes pour la représentation des données. *)
let word_size   = 4
let data_size   = word_size

let not_implemented() = failwith "Not implemented"
let not_implemented_compiler() = failwith "Not implemented in compile_expr"
let not_implemented_compiler_list() = failwith "Not implemented in compile_instr_list"

(* Les fonctions [push], [peek] et [pop] sont là pour vous aider à manipuler
   la pile. *)
  
(* val push : register -> text 
  [push reg] place le contenu de [reg] au sommet de la pile.
  $sp pointe sur l'adresse de la dernière case occupée. *)
let push reg =
  sub sp sp oi word_size
  @@ sw reg areg (0, sp)

(* val peek : register -> text
  [peek reg] place la valeur en sommet de pile dans [reg] sans dépiler. *)
let peek reg =
  lw reg areg (data_size - 4, sp)

(* val pop : register -> text
  [pop reg] place la valeur en sommet de pile dans [reg] et dépile. *)
let pop reg =
  peek reg
  @@ add sp sp oi data_size



    
(* La fonction de compilation des expressions prend en argument :
   l'expression à compiler. *)

   
let rec compile_expr e =
        
  match e.expr with

    | Econst c -> begin
      match c with
	| Cunit       -> push zero
	  
	(* À compléter avec les autres formes de constantes. *)
	
	| Cint value -> 
                li a0 value
                @@ push a0
                    
                            

	| _           -> not_implemented_compiler() 
    end
    
    | Eunop (Uminus, e) ->  let e_code = compile_expr e in
    e_code
      @@ pop a0
      @@ sub a0 zero oreg a0
      @@ push a0
    | Eprint_newline e ->
      let e_code = compile_expr e in
      e_code
      @@ jal "print_newline"
    | Eprint_int e ->
      let e_code = compile_expr e in
      e_code
      @@ jal "print_int"
    | Ebinop (Badd, e1, e2) ->
      let e1_code = compile_expr e2 in let e2_code = compile_expr e1 in
      e1_code 
      @@ e2_code
      @@ pop a0
      @@ pop a1
      @@ add a0 a0 oreg a1
      @@ push a0
    | Ebinop (Bsub, e1, e2) ->
      let e1_code = compile_expr e2 in let e2_code = compile_expr e1 in
      e1_code 
      @@ e2_code
      @@ pop a0
      @@ pop a1
      @@ sub a0 a0 oreg a1
      @@ push a0
    | Ebinop (Bmul, e1, e2) ->
      let e1_code = compile_expr e2 in let e2_code = compile_expr e1 in
      e1_code 
      @@ e2_code
      @@ pop a0
      @@ pop a1
      @@ mul a0 a0 oreg a1
      @@ push a0
    | Ebinop (Bdiv, e1, e2) ->
      let e1_code = compile_expr e2 in let e2_code = compile_expr e1 in
      e1_code 
      @@ e2_code
      @@ pop a0
      @@ pop a1
      @@ div a0 a0 oreg a1
      @@ push a0  
    | _ -> not_implemented_compiler()
    
	


      
(* Les instructions sont calculées l'une après l'autre. *)
let rec compile_instr_list il =
  match il with
    | []       -> nop

    (* À compléter pour le cas [Icompute] et l'itération. *)
    |  Icompute e :: dl -> (compile_expr e) @@ (compile_instr_list dl)
    | _ -> not_implemented_compiler_list()



      
(* Fragments de code pour [print_int] et [print_newline]. *)
let built_ins () =
  label "print_newline"
  @@ pop zero
  @@ li v0 11
  @@ li a0 10
  @@ syscall
  @@ push zero
  @@ jr ra
  @@ label "print_int"
  @@ pop a0
  @@ li v0 1
  @@ syscall
  @@ jr ra


    
(* La compilation du programme produit un code en trois parties :
   1/ Le code principal (label "main")
   2/ Une instruction de fin (label "end_exec")
   3/ Le code des fonctions d'affichage (built_ins_code)
*)
let compile_prog p =
  let main_code = compile_instr_list p in
  let built_ins_code = built_ins () in
  
  { text =
     comment "Code principal"
  @@ label "main"
  @@ main_code
       
  @@ comment "Fin"
  @@ label "end_exec"
  @@ li v0 10
  @@ syscall
       
  @@ comment "Primitives"
  @@ built_ins_code
  ;
    
    data = nop
  }
