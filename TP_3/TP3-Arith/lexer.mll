{

  open Lexing
  open Parser
  open Ast
  open Error

  (* Petite fonction utile pour la localisation des erreurs. *)
  let current_pos b =
    lexeme_start_p b,
    lexeme_end_p b

}



rule token = parse

  | '\n'
      { new_line lexbuf; token lexbuf }
  | ' ' {token lexbuf}
  | "(*" {comment lexbuf}

  | "print_newline"
      { PRINT_NEWLINE }
  | "()"
      { CONST_UNIT }
  | "print_int"
      { PRINT_INT }
  | '+'
      {PLUS}
  | '-'
      {MINUS}
  | '*'
      {STAR}
  | '/'
      {SLASH}
  | '('
      {LPAREN}
  | ')'
      {RPAREN}
  | ";;"
      {EOI}
  | ['0'-'9']+ as constant_entiere
      {CONST_INT (int_of_string( constant_entiere))} 
      


      
  (* Les autres caractères sont des erreurs lexicales. *)
  | _
      { error (Lexical_error (lexeme lexbuf)) (current_pos lexbuf) }
  (* Fin du fichier. *)
  | eof
      { EOF }

and comment = parse
    | "*)" { token lexbuf}
    
    | _ {comment lexbuf}
    
    | eof
        {failwith "End of file while comment not finished"}
    




